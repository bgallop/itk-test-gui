const loader_box = document.getElementById('loader_box')
const content_box = document.getElementById('content')

loader_box.style.display = 'none'

var Anchors = document.getElementsByTagName("a");

for (var i = 0; i < Anchors.length ; i++) {
    Anchors[i].addEventListener("click",
        function (event) {
            loader_box.style.display = 'inline-block'
            content_box.style.display = 'none'
        },
        false);
}

// link.addEventListener('click', ev => {
//     // loader_box.classList.remove('not-visible')
//     // content_box.classList.add('not-visible')
//     loader_box.style.display = 'inline-block'
//     content_box.style.display = 'none'
// })