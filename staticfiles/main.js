const form = document.getElementById('form')
const loader_box = document.getElementById('loader_box')
const content_box = document.getElementById('content')

loader_box.style.display = 'none'

form.addEventListener('submit', ev => {
    // loader_box.classList.remove('not-visible')
    // content_box.classList.add('not-visible')
    loader_box.style.display = 'inline-block'
    content_box.style.display = 'none'
})