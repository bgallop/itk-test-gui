# ITk module test GUI

This runs the web GUI as a Django server.

## Run locally

For testing, you can run the project locally. You will also want to use
the debug settings:

```
export DJANGO_SETTINGS_MODULE=djangoWebGUI.settings_dev
python manage.py runserver
```

Also see the end of that [file](djangoWebGUI/settings_dev.py) for info on
logging. More information can be found [here](https://docs.djangoproject.com).

## Deploy to Openshift

This is currently semi-manual. You'll first need to go to the page for the
BuildConfig and use the "Start build" action. This uses the basic Python
source to image process which knows how to install everything and also 
to run the Django server.

If the model has been updated, you'll also need to open terminal on the
server and do the following:

```
python manage.py makemigrations
python manage.py migrate
```

## Dockerfile
Added dockerfile for building webapp, at this stage will automatically run the debug server listening on all IPs on port 8000.
`.dockerignore` is nearly a copy of `.gitignore`, and will ignore certain files when copying into container
To build, from same directory as `Dockefile`:
* `docker build -t IMAGE_NAME .`, will create new image with tag `IMAGE_NAME`
To run:
* `docker run -it --rm -p HOST_PORT:8000 IMAGE_NAME`, assuming image name of `IMAGE_NAME`. `-it` sets up inputs and teletype, `-rm` removes the started container when finished. `-p` binds `HOST_PORT` on the host to port 8000 in the container
* `docker run -it --rm -p HOST_PORT:8000 -v WEBAPP_GIT_PATH/:/itk-test-gui IMAGE_NAME` is similar to above, but shadows WEBAPP_GIT_PATH into the container at runtime, so that you can run with the restarting debug server.

This could be used for local development if appropriate, or to start pushing towards a multiple container service

## More info

This was originally part of an ATLAS Qualification task, more information
provided here:

 https://cds.cern.ch/record/2788956

## Brief Overview

Application code is in display_data. Incoming URLs are mapped to functions
in urls.py. These functions are implemented in views.py.

HTML templates are in the templates directory, which display data created
by the view functions.

