"""

Views module for ITK web app
"""

import datetime
import logging
import pprint
import re
import sys
import time
import urllib

import display_data.do_stuff
import display_data.hybrid_summary_test as hybrid_summary_test
import display_data.module_summary_test as module_summary_test
import django.template.defaulttags
import django.urls
import django.utils.html
import django.http
import numpy as np
from display_data.bad_channel_tracing import Defect_Mapping
from display_data.dbAccess import _SITE_URL
from display_data.do_plotting_stuff import get_plot
from display_data.do_stuff import (get_associated_tests,
                                   list_components,
                                   list_institute_components, list_institutes,
                                   store_component_from_db_in_model,
                                   upload_test_result)
from display_data.models import ComponentInfo, Institute, TestSummary
from display_data.plotting import (atlas18_cv_test, atlas18_shape_metrology,
                                   full_strip_test, manufactering_data_atlas18,
                                   response_curve, strobe_delay,
                                   three_point_gain)
from display_data.specific_stuff import interesting_ct_codes
from display_data import pager
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.views.decorators.http import (require_http_methods, require_GET,
                                          require_POST)

from . import app_settings
from .forms import AccessForm, ComponentIDForm, ListFilterForm, OpenidForm, TokenForm, Upload_Test
from display_data import models as web_app_models

logger = logging.getLogger(__name__)

id_re = re.compile("[\da-f]{32}$", re.IGNORECASE)
# Match only Strips components
sn_re = re.compile("20US[A-Z]{2}[\dA-Z][\d]{7}$")

def comp_valid(some_id):
    if id_re.match(some_id):
        return True
    if sn_re.match(some_id):
        return True
    return False

@require_GET
def index(request):
    """
    Main home page
    """

    ctxt = {'example_list': [
        {"id": "20USGAM1002094", "desc": "An AMAC"},
        {"id": "20USGAH1003118", "desc": "An HCC"},
        {"id": "20USGWA1222000", "desc": "An ABC"},
        {"id": "20USBML1234841", "desc": "An LS Module"},
        {"id": "20USBHX2001244", "desc": "An X hybrid"},
        {"id": "20USBLC2000007", "desc": "An LS Stave"},
        {"id": "20USELC1000031", "desc": "A Petal"},
      ]}

    return render(request, 'index.html', ctxt)


@require_GET
def upload_successful(request):
    return render(request, 'upload_successful.html')


@require_GET
def plotting_example(request):
    x = [1, 2, 3, 4]
    y = [1, 2, 3, 4]
    plot = get_plot(x, y)
    context = {}
    context['plot'] = plot
    return render(request, 'plotting_test.html', context)


def do_auth(request, key1, key2):
    if len(key1) == 0 or len(key2) == 0:
        # Empty, don't try connecting
        messages.add_message(request, messages.WARNING,
                             'Authentication failed')
        return None

    # If we're given keys, use them to get token even if we have one already
    failed, token = display_data.do_stuff.access(key1, key2)
    if failed is True:
        logger.error("Authentication failed")
        messages.add_message(request, messages.WARNING,
                             'Authentication failed')

        try:
            del request.session["session_db_token"]
        except KeyError:
            pass
        try:
            del request.session["session_db_expires_at"]
        except KeyError:
            pass

        return None
    request.session["session_db_token"] = token
    now = time.time()
    # Valid for 30 mins
    request.session["session_db_expires_at"] = int(now + 25 * 60)

    return token

class NoTokenException(Exception):
    pass

def check_auth(request):
    try:
        s_expires = request.session["session_db_expires_at"]
        if time.time() > int(s_expires):
            logger.error("Authentication expired (%s vs %s)" %
                             (time.time(), s_expires))
            messages.add_message(request, messages.WARNING,
                                 'Authentication expired')
            raise NoTokenException()

        token = request.session["session_db_token"]
    except KeyError:
        logger.error("Authentication needed")
        messages.add_message(request, messages.WARNING,
                             'No authentication provided')
        raise NoTokenException()

    return token


def have_auth(request):
    have_auth = False
    try:
        request.session["session_db_token"]
        s_expires = request.session["session_db_expires_at"]
        if time.time() < int(s_expires):
            have_auth = True
    except KeyError:
        pass

    return have_auth

def wrap_auth(func):
    """
    Decorator to redirect to login page on failed auth.

    Note that we don't call check_auth here as it might not be needed.
    """

    def wrapped(request, *args, **kwargs):
        try:
            return func(request, *args, **kwargs)
        except NoTokenException:
            return HttpResponseRedirect(django.urls.reverse('display_data:login') + "?" + urllib.parse.urlencode({"redirect": request.get_full_path_info()}))

    return wrapped


@require_GET
def do_openid_receive(request):
    # Receive the token from remote server
    form = OpenidForm(request.GET)

    if not form.is_valid():
        logger.info("Open ID form results is not valid")

        messages.add_message(
            request, messages.WARNING, 'Login failed')

        return HttpResponseRedirect('/')

    new_token = form.cleaned_data.get('id_token')
    exp = int(form.cleaned_data.get('expires_in'))

    request.session["session_db_token"] = new_token
    now = time.time()
    request.session["session_db_expires_at"] = int(now + exp)

    return HttpResponseRedirect('/')

@require_http_methods(["GET", "POST"])
def login_page(request):
    logger.debug("Login page method %s" % request.method)

    # This should report the full host name
    server_host = request.get_host()

    # If we're running on dev (locally), open id just works
    allow_using_openid = "localhost" in server_host
    if app_settings.OPENID_CLIENT_ID:
        allow_using_openid = True

    if request.method == 'GET':
        destination = request.GET.get("redirect", default = "/")
        if have_auth(request):
            messages.add_message(
                request, messages.WARNING, 'Already logged in')
            return HttpResponseRedirect(destination)

        form = AccessForm(initial = {"redirect": destination})

        context = {
            'form': form,
        }

        context["do_openid"] = allow_using_openid
        if allow_using_openid:
            context["client_id"] = app_settings.OPENID_CLIENT_ID
            if app_settings.OPENID_CLIENT_SECRET:
                # On localhost, we don't need secret
                context["client_secret"] = app_settings.OPENID_CLIENT_SECRET
            # Name that gets redirected to do_openid_receive
            context['receiver'] = 'receiver'
            context["scope"] = "openid https://itkpd-test.unicorncollege.cz"
            context["base_oidc"] = "https://uuidentity.plus4u.net/uu-oidc-maing02/bb977a99f4cc4c37a2afce3fd599d0a7/oidc/auth?acrValues=standard high veryHigh"

        return render(request, 'login.html', context)

    # Get form data from the request
    form = AccessForm(request.POST)
    if not form.is_valid():
        messages.add_message(request, messages.WARNING, 'Invalid form data')
        return HttpResponseRedirect('/')

    key1, key2 = form.cleaned_data.get('key1'), form.cleaned_data.get('key2')
    token = do_auth(request, key1, key2)

    if token is not None:
        messages.add_message(request, messages.WARNING, 'Login successful')

    destination = form.cleaned_data.get('redirect', '/')

    return HttpResponseRedirect(destination)

@require_http_methods(["GET", "POST"])
def login_token_page(request):
    logger.debug("Login via token method: %s" % request.method)

    if request.method == 'GET':
        if have_auth(request):
            messages.add_message(
                request, messages.WARNING, 'Already logged in')
            return HttpResponseRedirect('/')

        form = TokenForm()

        return render(request, 'login_token.html',
                      {'form': form})


    # Get form data from the request
    form = TokenForm(request.POST)
    if not form.is_valid():
        messages.add_message(request, messages.WARNING, 'Invalid form data')
        return HttpResponseRedirect('/')

    new_token = form.cleaned_data.get('db_token')

    if new_token is not None:
        messages.add_message(request, messages.WARNING, 'Login successful')
    else:
        messages.add_message(request, messages.WARNING, 'Login not successful')
        return HttpResponseRedirect('/')

    # Should do some validation
    request.session["session_db_token"] = new_token
    # Valid for 30 mins
    now = time.time()
    request.session["session_db_expires_at"] = int(now + 25 * 60)

    return HttpResponseRedirect('/')

@require_http_methods(["GET", "POST"])
@wrap_auth
def upload_test(request):
    if request.method == 'POST':
        form = Upload_Test(request.POST, request.FILES)
        if form.is_valid():
            component_ID = form.cleaned_data.get('Component_ID')
            if id_re.match(form.cleaned_data.get('Component_ID')):
                messages.add_message(request, messages.WARNING, 'Bad component ID (expecting 32 hexdigit code)')
                return HttpResponseRedirect('upload')
            comments = form.cleaned_data.get('comments')
            institution = form.cleaned_data.get('institution')
            # do things
            token = check_auth(request)

            try:
                upload_test_result(token,
                    request.FILES['file'], component_ID, institution, comments)
            except Exception:
                messages.add_message(
                    request, messages.WARNING, 'Upload failed (invalid ID, or file?)')
                return HttpResponseRedirect('upload')
            return HttpResponseRedirect('upload_successful')
    else:
        form = Upload_Test()

    return render(request, 'upload_test_form.html', {'form': form})


def find_component(component_id_or_str):
    """
    Given DB id or ATLAS serial number, look up in component info in local DB

    Args: 
        component_id_str: A string generally taken from a webform representing the database ID or
            the ATLAS serial number
    Returns:
        Either the component model in the local database or None
    """
    # Fallback serial number regex to match according to defn given.
    # Matches with capture groups on project/subject code, sub project specific identifiers and
    # 7 digit serial code
    # TODO: Check whether subject project identifier should be strictly letters only, and regex
    # defn
    if id_re.match(component_id_or_str):
        component = ComponentInfo.objects.get(
            component_ID=component_id_or_str
        )
    elif sn_re.match(component_id_or_str):
        component = ComponentInfo.objects.get(
            serialNumber=component_id_or_str
        )
    else:
        return None
    return component


@require_GET
@wrap_auth
def get_component_view(request, **kwargs):
    serial = kwargs.get('serial')

    token = check_auth(request)

    componentID = serial
    if not comp_valid(componentID):
        messages.add_message(request, messages.WARNING, 'Bad component ID (either length 14 serial number (20US...) or 32 hexdigit code)')
        return HttpResponseRedirect('CIDForm')

    # Look for component in local DB
    comp = find_component(serial)
    if comp is None:
        messages.add_message(request, messages.INFO, 'Fetch info from DB')

        # Check in remote DB
        comp, create, _ = store_component_from_db_in_model(serial, token)

        if comp is None:
            messages.add_message(request, messages.WARNING, 'Bad component, not found in DB')

            return HttpResponseRedirect('CIDForm')

    context = {
        "component": comp,
    }

    if comp.component_type == "Module":
        context['link_summary'] = (
            django.urls.reverse("display_data:module_summary_test_view", args=(comp.component_ID,)) + "?cached=True"
        )
    elif comp.component_type == "STAR Hybrid Assembly":
        context['link_summary'] = (
            django.urls.reverse("display_data:hybrid_summary_test_view", args=(comp.component_ID,)) + "?cached=True"
        )

    return render(request, 'component.html', context)


@require_GET
@wrap_auth
def request_component_tests(request, **kwargs):
    serial = kwargs.get('serial')

    token = check_auth(request)

    componentID = serial
    if not comp_valid(componentID):
        messages.add_message(request, messages.WARNING, 'Bad component ID (either length 14 serial number (20US...) or 32 hexdigit code)')
        return HttpResponseRedirect('CIDForm')

    try:
        componentID = get_associated_tests(componentID, token)
    except KeyError as ke:
        logger.error(f"Can't get associated tests due to key {ke}")
        messages.add_message(request, messages.WARNING,
                                 'Failed to get associated tests')
        return HttpResponseRedirect('')
    # redirect to a new URL:
    return redirect(f'/tests/{componentID}')


@require_http_methods(["GET", "POST"])
@wrap_auth
def get_component_ID(request):
    """
    View for page that allows user to lookup component by ATLAS serial number or database ID
    """
    if request.method == 'GET':
        if "Component_ID" in request.GET:
            form = ComponentIDForm(request.GET)
        else:
            form = ComponentIDForm(initial = {"without_request": True})

        return render(request, 'ComponentID.html', {'form': form})

    # if this is a POST request we need to process the form data
    elif request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ComponentIDForm(request.POST)

        logger.info("Posting get_component_ID")

        # check whether it's valid:
        if not form.is_valid():
            logger.error("Form is not valid")
            messages.add_message(
                request, messages.WARNING, 'Invalid form data')
            return HttpResponseRedirect('CIDForm')
        logger.error("Form is valid")

        user_submitted_id = form.cleaned_data.get('Component_ID')

        if not comp_valid(user_submitted_id):
            if not (len(user_submitted_id) == 32 or len(user_submitted_id) == 14):
                logger.error("Input does not have correct length!")
            else:
                logger.error("Input does not match!")
                
            messages.add_message(request, messages.WARNING, 'Bad component ID (either length 14 serial number (20US...) or 32 hexdigit code)')
            return HttpResponseRedirect('CIDForm')

        # Lookup component
        if form.cleaned_data.get('without_request') is True:
            # If requested by user, check whether the component is already in the SQLite database
            logger.error('Lookup component local')
            # Fallback to GET if neither options true
            try:
                component = find_component(user_submitted_id)
            except django.core.exceptions.ObjectDoesNotExist:
                component = None
            if component:
                # This is relevant to convert the ATLAS serial number into component ID to
                # redirect to the correct url
                component_id = component.component_ID
            else:
                component_id = None
        else:
            token = check_auth(request)
            try:
                component_id = get_associated_tests(user_submitted_id, token)
            except KeyError as error:
                # This is a part of the error handling: assume that if there's an issue with 
                # requesting from ITK DB or in this webform, that at some point an key error will 
                # be thrown. This will be caught here, and then the fault diagnosed in the next 
                # section. This is important for automated pipeline testing.
                print(f"Exception with getting associated tests: missing key {error}")
                component_id = None

        # Now that we have the database ID of the component, and/or filled up the local database,
        # redirect to the appropriate page
        if component_id is None:
            # Different warning/message depending on whether local DB was used
            if form.cleaned_data.get('without_request'):
                messages.add_message(
                    request, messages.WARNING,
                    'No cached information on submitted component %s' % user_submitted_id
                )
            else:
                logger.error("Can't get associated tests")
                messages.add_message(
                    request, messages.WARNING,
                    'Failed to get tests associated with component'
                )
            return HttpResponseRedirect('CIDForm')
        else:
            # Based on the button pressed, go to component status summary or tree view
            if "get_test_summary" in request.POST:
                return HttpResponseRedirect(
                    django.urls.reverse(
                        "display_data:test results",
                        args=[component_id, ],
                    ),
                )
            elif "get_tree_view" in request.POST:
                # Unclear if there's a better way to combine args with query params
                redirect_url = django.urls.reverse(
                    "display_data:defect_tree_view",
                    args=[component_id, ],
                )
                if form.cleaned_data.get('without_request') is True:
                    redirect_url = redirect_url + "?cached=True"
                return HttpResponseRedirect(redirect_url)
            elif "get_module_summary_test" in request.POST:
                print("This is a POST request to get module summary test")
                print(f"Request is {request}")
                redirect_url = django.urls.reverse(
                    "display_data:module_summary_test_view",
                    args=[component_id, ],
                )
                if form.cleaned_data.get('without_request') is True:
                    redirect_url = redirect_url + "?cached=True"
                return HttpResponseRedirect(redirect_url)
            elif "get_hybrid_summary_test" in request.POST:
                redirect_url = django.urls.reverse(
                    "display_data:hybrid_summary_test_view",
                    args=[component_id, ],
                )
                if form.cleaned_data.get('without_request') is True:
                    redirect_url = redirect_url + "?cached=True"
                return HttpResponseRedirect(redirect_url)
            else:
                # Default behaviour, redirect to original display_data:test_results view
                return HttpResponseRedirect(
                    django.urls.reverse(
                        "display_data:test results",
                        args=[component_id, ],
                    ),
                )

@require_POST
@wrap_auth
def fetch_institutions(request):
    # Check for valid authentication
    token = check_auth(request)

    try:
        list_institutes(token)
        return HttpResponseRedirect('listInst')
    except Exception as e:
        sys.stderr.write("Error (maybe from DB) listing institutes: %s\n" % e)
        messages.add_message(request, messages.WARNING,
                             'Error from DB listing institutes')
        return HttpResponseRedirect('/')


@require_GET
def list_institutions(request):
    insts = Institute.objects.all()

    empty_inst = len(insts) == 0

    context = {'insts': insts, "empty_inst": empty_inst}

    return render(request, 'list_insts.html', context)


@require_GET
def view_institution(request, **kwargs):
    inst_code = kwargs.get('inst_code')

    inst_list = Institute.objects.filter(inst_code=inst_code)
    if len(inst_list) != 1:
        logger.error("Bad inst code %s" % inst_code)
        messages.add_message(request, messages.WARNING,
                             'Bad code %s' % inst_code)
        return HttpResponseRedirect('/listInst')

    inst = inst_list[0]

    comp_types = interesting_ct_codes

    context = {'inst': inst,
               'comp_types': comp_types}
    return render(request, 'view_inst.html', context)


@require_GET
@wrap_auth
def view_institution_comp_type(request, **kwargs):
    inst_code = kwargs.get('inst_code')
    ct_code = kwargs.get('ct_code')

    token = check_auth(request)

    comp_list = list_institute_components(token, inst_code, ct_code)
    have_list = []
    missing_list = []

    # Which do we have data for
    for s in comp_list:
        if ("sn" in s and
                len(ComponentInfo.objects.filter(serialNumber=s["sn"])) == 1):
            have_list.append(s)
        elif len(ComponentInfo.objects.filter(component_ID=s["code"])) == 1:
            have_list.append(s)
        else:
            missing_list.append(s)

    form = ComponentIDForm({"Component_ID": ct_code, "without_request": True})

    context = {'inst_code': inst_code,
               'ct_code': ct_code,
               'have_list': have_list,
               'missing_list': missing_list,
               'form': form
               }

    return render(request, 'inst_comp_list.html', context)


@require_GET
@wrap_auth
def view_comp_table(request, **kwargs):
    token = check_auth(request)

    pick_comp = request.GET.get("comp_pick")

    keys = {
        # Default to something instead of all components
        "comp_type": "ABC",
    }

    if pick_comp in interesting_ct_codes:
        keys["comp_type"] = pick_comp

    # Vaguely copied from pager.py (NB 1-based)
    def validate_page_number(n):
        try:
            if isinstance(n, float) and not n.is_integer():
                raise ValueError
            number = int(n)
        except (TypeError, ValueError):
            number = 1
        if number < 1:
            number = 1
        return number

    user_page_number = request.GET.get("page")
    user_page_number = validate_page_number(user_page_number)

    # Give 0-based number to API
    page_number = user_page_number - 1

    page_size = 10

    comp_model_list, total_count = list_components(
        token, page_index=page_number, page_size=page_size, **keys
    )

    my_pager = pager.VirtualPaginator(page_size, total_count = total_count)
    my_page = my_pager.get_page(user_page_number)

    column_names = ["DB ID",
                    "SN",
                    "Alt ID",
                    "Stage",
                    "Batch"]
    column_access = ["component_ID",
                     "serialNumber",
                     "alternative_ID",
                     "current_stage",
                     "batch_name"]

    comp_list = [
        {
            "code": component.component_ID,
            "table_data": [
                getattr(component, col_acc)
                for col_acc in column_access
            ]
        }
        for component in comp_model_list
    ]

    form_info = {}
    if "comp_type" in keys:
        form_info["comp_pick"] = keys["comp_type"]

    logger.debug(f"Fill form {form_info}")
    choices = {
        "comp_pick":
        [
            (c, c) for c in interesting_ct_codes
        ],
    }

    form = ListFilterForm(initial = form_info, choices = choices)
    # Simple form on one line
    form = form.render("simple_form.html")
    context = {
        "comp_list": comp_list,
        "col_list": column_names,
        "col_params": column_access,
        "page_obj": my_page,
        "filter_form": form,
    }

    return render(request, "comp_list_table.html", context)


# Summary of test info for one component


@require_GET
def test_summary(request, component_id_or_sn):
    try:
        comp = find_component(component_id_or_sn)
        if comp is None:
            messages.add_message(request, messages.WARNING,
                                 'Bad component %s' % component_id_or_sn)
            return HttpResponseRedirect('/')

        component = comp
    except django.core.exceptions.ObjectDoesNotExist:
        # sys.stderr.write("Bad inst code %s\n" % inst_code)
        messages.add_message(request, messages.WARNING,
                             'Code %s not in cache' % component_id_or_sn)
        return HttpResponseRedirect('/')

    componentID = component.component_ID

    # latest_test_list = TestSummary.objects.order_by('-id')
    latest_test_list = TestSummary.objects.filter(
        component_ID=componentID, child=False)

    D = Defect_Mapping()
    D.get_defects_sensor(componentID)
    D.get_defects_hybrid(componentID)

    class Defect_summary():
        def __init__(self, name, segment, strip, channel, chip_bank, chip_index, description, test):
            self.name = name
            self.segment = segment
            self.strip = strip
            self.channel = channel
            self.chip_bank = chip_bank
            self.chip_index = chip_index
            self.description = description
            self.test = test

    defect_list = []
    for i in range(0, len(D.name)):
        defect_list.append(Defect_summary(D.name[i], D.segment[i], D.strip[i], D.channel[i], D.chip_bank[i],
                                          D.chip_index[i], D.description[i], D.test[i]))
    child_tests = []
    for i in range(0, component.nChildren):
        child = TestSummary.objects.filter(
            component_ID=componentID, child=True, child_layer=1, child_number=i+1)
        if child:
            child_tests.append(child)

    context = {'latest_test_list': latest_test_list,
               'component_type': component.component_type,
               'type': component.type,
               'serialNumber': component.serialNumber,
               'componentID': component.component_ID,
               'parents_ID': component.parents_ID,
               'parents_type': component.parents_type,
               'parents_serial': component.parents_serial,
               'defect_list': defect_list,
               'child_tests': child_tests}

    # Stored comp name not code...
    if component.component_type == "Module":
        context['link_summary'] = (
            django.urls.reverse("display_data:module_summary_test_view", args=(component.component_ID,)) + "?cached=True"
        )
    elif component.component_type == "STAR Hybrid Assembly":
        context['link_summary'] = (
            django.urls.reverse("display_data:hybrid_summary_test_view", args=(component.component_ID,)) + "?cached=True"
        )
    else:
        logger.info(f"No special link for type {component.component_type}")

    return render(request, 'testResults.html', context)


@require_GET
def tests_details(request, **kwargs):
    count = kwargs.get('count')

    component_id_or_sn = kwargs.get('component_id_or_sn')

    if not comp_valid(component_id_or_sn):
        messages.add_message(request, messages.WARNING, 'Bad component %s' % component_id_or_sn)
        return HttpResponseRedirect('/')

    try:
        component = find_component(component_id_or_sn)
    except django.core.exceptions.ObjectDoesNotExist:
        messages.add_message(request, messages.WARNING,
                             'Code %s not recognised' % component_id_or_sn)
        return HttpResponseRedirect('/')
        

    if component is None:
        messages.add_message(request, messages.WARNING,
                             'Bad code for test details %s' % component_id_or_sn)
        return HttpResponseRedirect('/')

    try:
        componentID = component.component_ID
    except IndexError:
        messages.add_message(request, messages.WARNING,
                             'Code %s not recognised' % component_id_or_sn)
        return HttpResponseRedirect('/')

    try:
        test = TestSummary.objects.get(count=count, component_ID=componentID)
    except TestSummary.DoesNotExist:
        messages.add_message(request, messages.WARNING,
                             'Missing code/test %s/%s' % (componentID, count))
        return HttpResponseRedirect(f'/tests/{componentID}')

    test_content = test.test_content
    results_list = test_content['results']
    properties = test_content['properties']
    comments = test_content['comments']

    if results_list is None:
        # This shouldn't happen
        messages.add_message(request, messages.WARNING,
                             'Results is None in TestResult')
        results = {}
    else:
        results = {r["code"]: r["value"] for r in results_list}
    if properties is None:
        properties = []
    if comments is None:
        comments = []

    try:
        defects = test_content['defects']
    except KeyError:
        defects = []

    class Result():
        def __init__(self, name, value):
            self.name = name
            self.value = value

    class Defect():
        def __init__(self, name, description, properties):
            self.name = name
            self.description = description
            self.properties = properties

    class Head():
        def __init__(self, test):
            t = test.test_content
            self.id = t['id']
            self.state = t['state']
            self.date = t['date']
            self.componentType = t['components'][0]['componentType']['code']
            self.componentID = t['components'][0]['code']
            self.passed = t['passed']
            self.request_component_id = test.component_ID

    head = Head(test)
    result_list = []
    plot_list = []
    show_result_table = True
    voltage = None
    current = None

    test_list_for_plotting = ['Three Point Gain', 'Response Curve',
                              'Strobe Delay', 'ATLAS18 Full Strip Std Test V1',
                              'Manufacturing Data ATLAS18', 'ATLAS18 CV Test V1',
                              'ATLAS18 Shape Metrology V1']

    for code, value in results.items():
        flag = True
        try:
            array = value
            x = np.arange(len(array)) + 1
            ylabel = code
            xlabel = ''
            if head.componentType == 'HYBRID':
                xlabel = 'ABC chip'
            if 'IV' in test.test_name:
                if code == 'VOLTAGE':
                    voltage = value
                    flag = False
                if code == 'CURRENT':
                    current = value
                    flag = False
            if test.test_name in test_list_for_plotting:
                flag = False
            if flag:
                if len(array) > 1:
                    plot = get_plot(x, array, ylabel=ylabel, xlabel=xlabel)
                    plot_list.append(plot)
                else:
                    result_list.append(Result(code, value))
        except Exception:
            result_list.append(Result(code, value))

    if voltage is not None and current is not None:
        plot = get_plot(
            voltage, current, ylabel='current I [A]', xlabel='voltage U [V]', figsize=(5, 3))
        plot_list.append(plot)

    if test.test_name == 'Three Point Gain':
        plot_list = three_point_gain(results)

    if test.test_name == 'Response Curve':
        plot_list = response_curve(results)

    if test.test_name == 'Strobe Delay':
        plot_list = strobe_delay(results)

    if test.test_name == 'ATLAS18 Full Strip Std Test V1':
        plot_list = full_strip_test(results)

    if test.test_name == 'Manufacturing Data ATLAS18':
        plot_list = manufactering_data_atlas18(results)

    if test.test_name == 'ATLAS18 CV Test V1':
        plot_list = atlas18_cv_test(results)

    if test.test_name == 'ATLAS18 Shape Metrology V1':
        plot_list = atlas18_shape_metrology(results)

    if len(result_list) == 0:
        show_result_table = False
    prop_list = []

    for p in properties:
        prop_list.append(Result(p['code'], p['value']))

    defects_list = []
    for d in defects:
        defects_list.append(
            Defect(d['name'], d['description'], d['properties']))

    comment_list = []

    for c in comments:
        comment_list.append(Result(c['dateTime'], c['comment']))

    context = {'test_detail': test, 'result_list': result_list,
               'prop_list': prop_list, 'head': head,
               'comment_list': comment_list,
               'plot_list': plot_list,
               'show_result_table': show_result_table,
               'defects_list': defects_list}
    return render(request, 'testResultsDetail.html', context)


@require_GET
def cached_summary(request):
    tests = TestSummary.objects.all()
    comps = ComponentInfo.objects.all()

    test_components = {t.component_ID:
                       {"id": t.component_ID,
                        "name": t.test_name} for t in tests}
    components = {c.component_ID:
                  {"id": c.component_ID,
                   "sn": c.serialNumber} for c in comps}

    test_comps = []
    for t in components.keys():
        dt = dict(components[t])
        if t in test_components:
            dt.update(test_components[t])
        test_comps.append(dt)

    context = {'test_comps': test_comps}
    return render(request, 'summary.html', context)


@require_GET
@wrap_auth
def defect_tree_view(request, component_id):
    """
    A function to create a view of a defect tree for a given thing

    CID form looks up a component on the DB, then passes off to tests/<id>

    Using:
    * https://www.w3schools.com/howto/howto_js_treeview.asp
    * Might also be nice to use the layout from liquipedia
    """

    # TODO: Find better way to flip using cache on or off, but whatever
    query_keys = [i for i in request.GET.keys()] + \
        [i for i in request.POST.keys()]
    if "cached" in [i.lower() for i in query_keys]:
        use_cache = True
    else:
        use_cache = False

    # Used to return the query time at the end
    time_start = datetime.datetime.now()

    # Should be be able to lookup via ID and local db, assuming we came from CID page
    component = find_component(component_id)

    # Grab the token, if we need it
    if not use_cache:
        token = check_auth(request)
    else:
        token = None

    if not use_cache:
        print("Not using cache, refresh local db")
        component, created, api_response = display_data.do_stuff.store_component_and_children_in_db(
            component_id, token
        )
    else:
        component = ComponentInfo.objects.get(component_ID=component_id)

    # Placeholder: some generated text outside of templates
    block_text = django.utils.html.format_html(f"""
    <p>
        Component ID:{component_id}, Component: {component}
    <p>
    
    <p>
        New db query is {component}
    </p>
    """)

    def tree_build(component_id):
        """
        To recursively build tree, and assign to parent node

        This is just a tree of { "name" : name, "children" : [children nodes], "component_id" : component_id}
        It's expected that this is just used to set up the relationships and data will be pulled from DB as req
        """
        this_component = ComponentInfo.objects.get(component_ID=component_id)

        this_node = {
            "children": [],
            "component_id": this_component.component_ID,
            "verbose_type": this_component.type
        }
        this_name = f"{this_component.component_type}:{this_component.serialNumber}"
        this_node["name"] = this_name
        # Look up the children with this as parent
        these_children = ComponentInfo.objects.filter(
            parent_component=this_component).all()
        for child in these_children:
            # For each child, we need to build a new node
            child_node = tree_build(child.component_ID)
            this_node["children"].append(child_node)
        return this_node
    component_tree = tree_build(component_id)

    if not use_cache:
        # get_test_ids_for_component_tree will query the ITK DB using the component IDs in the tree, and return a list of all associated test ids
        test_ids = display_data.do_stuff.get_test_ids_for_component_tree(
            component_tree, token)
        # After we have a list of all test ids that represent any and all tests performed on the module and subcomponents we sync them to the DB
        display_data.do_stuff.sync_tests_to_db(test_ids, token)
    else:
        # Using cache here
        pass
    # So at this point we have all the tests assoc to each component synced

    def get_component_ids(tree_node, acc=[]):
        """

        """
        result = []
        result = result + [tree_node["component_id"]]
        for child in tree_node["children"]:
            update = get_component_ids(child)
            result = result + update
        return result

    component_ids = get_component_ids(component_tree)
    test_summary_dict = {}
    for component_id in component_ids:
        # Can lookup based on component ID stored in the test summary, but ideally we should look up component and then do a join
        # Double underscore used to get Django to query across joins
        assoc_tests = TestSummary.objects.filter(
            associated_component__component_ID=component_id).select_related()
        test_summary_dict[component_id] = {
            "test_count": len(assoc_tests),
            "passing_tests": assoc_tests.filter(passed=True),
            "failing_tests": assoc_tests.filter(passed=True),
        }
    # TODO: Django templates won't process dictionary keys as you'd expect, and operating similar to native python is a wontfix
    # Thus we'll need to either implement a template filter to do this in the postproc, or attach to the tree
    # See: https://stackoverflow.com/questions/8000022/django-template-how-to-look-up-a-dictionary-value-with-a-variable, https://code.djangoproject.com/ticket/3371

    def add_tests_to_tree(component_tree):
        """

        """
        assoc_tests = TestSummary.objects.filter(
            associated_component__component_ID=component_tree["component_id"]).select_related()
        component_tree["test_count"] = len(assoc_tests)
        component_tree["passing_tests"] = assoc_tests.filter(passed=True)
        component_tree["failing_tests"] = assoc_tests.filter(passed=False)
        component_tree["number_tests_failing"] = len(
            component_tree["failing_tests"])
        component_tree["number_tests_passing"] = len(
            component_tree["passing_tests"])
        for child in component_tree["children"]:
            add_tests_to_tree(child)
        return
    add_tests_to_tree(component_tree)

    # Want to also add some extra stuff to the tree, such as formatting nice links to ITK db
    def add_formatting_to_tree(component_tree):
        """

        """
        # TODO: Appropriate way to build URL and sanitise inputs
        base_url = "https://itkpd-test.unicorncollege.cz/componentView"
        url_params = {"code": component_tree["component_id"]}
        component_tree["itk_db_link"] = base_url + \
            "?" + urllib.parse.urlencode(url_params)
        for child in component_tree["children"]:
            add_formatting_to_tree(child)
        return
    add_formatting_to_tree(component_tree)

    # We now have a tree of componennts, consider pulling tests
    # We'll pass the component tree to another function that will fill up the DB

    time_end = datetime.datetime.now()
    context = {
        "block_text": block_text,
        "root_node": component_tree,
        "view_run_time": time_end - time_start,
        "test_summary_dict": test_summary_dict,
    }
    print("Render the tree view")
    return render(request, "tree_view.html", context)


# Adding a custom template filter so we can lookup dicts etc with a variable key
# https://stackoverflow.com/questions/8000022/django-template-how-to-look-up-a-dictionary-value-with-a-variable
@django.template.defaulttags.register.filter
def get_item(a_dict, key):
    """
    Using get on the dict makes it so that if the key doesn't exist none is returned, similar to
    usual django practice
    """
    return a_dict.get(key)


@require_GET
@wrap_auth
def module_summary_test_view(request, component_id):
    """
    Quick function to run the summary test
    """
    time_start = datetime.datetime.now()

    # Grab token
    token = check_auth(request)

    module_summary_test_result, component_api_response = module_summary_test.module_test_func(
        component_id, token)
    if module_summary_test_result["lookup_success"] is False:
        if module_summary_test_result["lookup_fault"] == "NOT_MODULE_TYPE":
            logger.error(f"Not module type {component_id}")
            messages.add_message(request, messages.WARNING,
                                 'Looked up component that isn\'t a module')
            return HttpResponseRedirect("/CIDForm")
        elif module_summary_test_result["lookup_fault"] == "uuAppErrorMap":
            logger.error(f"Issue with ITK DB, uuAppErrorMap {component_id}")
            messages.add_message(request, messages.WARNING,
                                 'Issue with ITK DB, uuAppErrorMap')
            return HttpResponseRedirect('/CIDForm')
    # TODO: Do some more catching of whether the test was carried out successfully, deal with DB
    # issues, etc,

    # Add/cache the module summary test, TODO: work in some flags about when/whether to set this
    parent_component = web_app_models.ComponentInfo.objects.get(component_ID=module_summary_test_result["component"])
    web_app_models.ModuleSummaryTest.objects.update_or_create(
        parent_component=parent_component,
        defaults={"summary_test_content":module_summary_test_result},
    )

    # TODO: See if there's a better way of constructing this, the requests might have something
    parse_result = urllib.parse.urlparse(
        urllib.parse.urljoin(_SITE_URL, "componentView"))
    query_params = urllib.parse.urlencode(
        {"code": module_summary_test_result["component"]})
    parse_result = parse_result._replace(query=query_params)
    component_db_link = urllib.parse.urlunparse(parse_result)

    test_summary_link = django.urls.reverse("display_data:test results", args=(component_id,))

    missing_tests = [i for (i, value) in module_summary_test_result["results"]
                     ["required_tests_done"].items() if not value]
    time_end = datetime.datetime.now()

    # Create a dict to link test run IDs to urls on DB
    test_id_to_url_dict = {}
    for _, (_, _, test_run_id) in module_summary_test_result["results"]["per_test_check"].items():
        parse_result = urllib.parse.urlparse(
            urllib.parse.urljoin(_SITE_URL, "testRunView"))
        query_params = urllib.parse.urlencode({"id": test_run_id})
        parse_result = parse_result._replace(query=query_params)
        test_run_link = urllib.parse.urlunparse(parse_result)
        test_id_to_url_dict[test_run_id] = test_run_link
    context = {
        "lookup_key": component_id,
        "time_start": time_start,
        "time_end": time_end,
        "runtime": time_end - time_start,
        "module_summary_test_result": module_summary_test_result,
        "component_api_response": component_api_response,
        "component_db_link": component_db_link,
        "test_summary_link": test_summary_link,
        "json_download_link" : django.urls.reverse("display_data:module_summary_test_json_view", args=(component_id, )),
        "missing_tests": missing_tests,
        "show_debug": app_settings.SHOW_MODULE_SUMMARY_DEBUG,
        "test_id_to_url_dict": test_id_to_url_dict,
    }

    return render(request, "module_summary_test.html", context)


@require_GET
def module_summary_test_json_view(request, component_id):
    """
    Serves a created module summary test
    https://stackoverflow.com/questions/56280289/django-create-and-output-txt-file-from-view
    """
    print(f"Component id: {component_id}")
    response = django.http.HttpResponse(content_type='text/plain')  
    response['Content-Disposition'] = f'attachment; filename="module_summary_test_{component_id }.txt"'
    this_report = web_app_models.ModuleSummaryTest.objects.get(
        parent_component=web_app_models.ComponentInfo.objects.get(component_ID=component_id),
    )
    response.write(this_report.summary_test_content)

    return response

@require_GET
@wrap_auth
def hybrid_summary_test_view(request, component_id):
    """
    Quick view to display the results of Sadia's hybrid summary test
    """
    print("Start hybrid summary test view")
    time_start = datetime.datetime.now()
    # Grab token
    token = check_auth(request)

    # Run the hybrid summary test or pull from cache
    this_hybrid_summary = hybrid_summary_test.hybrid_summary_test(component_id, token)

    # Deduce the link to the ITK DB, requires component code
    parse_result = urllib.parse.urlparse(
        urllib.parse.urljoin(_SITE_URL, "componentView"))
    query_params = urllib.parse.urlencode(
        {"code": component_id}
    )
    parse_result = parse_result._replace(query=query_params)
    component_db_link = urllib.parse.urlunparse(parse_result)

    test_summary_link = django.urls.reverse("display_data:test results", args=(component_id,))

    time_end = datetime.datetime.now()
    context = {
        "lookup_key": component_id,
        "time_start": time_start,
        "time_end": time_end,
        "runtime": time_end - time_start,
        "component_db_link": component_db_link,
        "test_summary_link": test_summary_link,
        "hybrid_summary" : this_hybrid_summary,
        "hybrid_summary_string" : pprint.pformat(this_hybrid_summary),
    }
    print("End hybrid summary test view")
    return render(request, "hybrid_summary_test.html", context)
