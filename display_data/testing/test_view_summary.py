from django.test import TestCase

from django.utils import timezone

import datetime

from display_data.models import ComponentInfo, TestSummary

oldtime = timezone.now() - datetime.timedelta(days=1)

# Testing lookup of component summary data
class SummaryLookupTest(TestCase):
    def check_redirect(self, response, check, to_login = False):
        chain = [('/', 302)]

        self.assertEqual(chain, response.redirect_chain)

        if check not in str(response.content):
            print(response.content)
        self.assertContains(response, check)

    def test_bad_name(self):
        response = self.client.get('/tests/012345', follow = True)
        self.check_redirect(response, "Bad component")

    def test_good_code_missing(self):
        response = self.client.get('/tests/01234567890123456789012345678901', follow = True)
        self.check_redirect(response, "not in cache")

    def test_good_sn_missing(self):
        response = self.client.get('/tests/20USZZ01234567', follow = True)
        self.check_redirect(response, "Code 20USZZ01234567 not in cache")

    def test_good_sn_with_YY_missing(self):
        # Second position of YY can be number
        response = self.client.get('/tests/20USEZ80123456', follow = True)
        self.check_redirect(response, "not in cache")

    def test_bad_sn_with_bad_YY(self):
        # YY can only have number in second position
        response = self.client.get('/tests/20USE880123456', follow = True)
        self.check_redirect(response, "Bad component")

    def test_good_missing(self):
        response = self.client.get('/tests/01234567890123456789012345678901', follow = True)
        self.check_redirect(response, "not in cache")

    def test_good_present(self):
        comp_id = '01234567890123456789012345678901'
        comp_sn = '20USWW01234567'
        ComponentInfo(component_ID = comp_id,
                      parents_serial = comp_sn,
                      pub_date = oldtime).save()
        response = self.client.get('/tests/%s' % comp_id, follow = True)

        check = comp_sn
        if check not in str(response.content):
            print(response.content)
        self.assertContains(response, check)

# Testing lookup of test data
class TestSummaryLookupTest(TestCase):
    def check_redirect(self, response, check, to_login = False):
        chain = [('/', 302)]

        self.assertEqual(chain, response.redirect_chain)

        if check not in str(response.content):
            print(response.content)
        self.assertContains(response, check)

    def check(self, cid, cnt, check):
        response = self.client.get(f'/test_detail/{cid}/{cnt}/', follow = True)
        self.check_redirect(response, check)

    def test_bad_name(self):
        self.check('012345', "1", "Bad component")
        self.check('012asd345', "1", "Bad component")

    def test_good_missing(self):
        self.check('20USWW01234567', "1", "not recognised")
        self.check('01234567890123456789012345678901', "1", "not recognised")

    def test_good_code_missing_test(self):
        self.check('20USWW01234567', "1", "not recognised")
        self.check('01234567890123456789012345678901', "1", "not recognised")

    def test_good_comp_present_no_test(self):
        comp_id = '01234567890123456789012345678901'
        comp_sn = '20USWW01234567'
        # Enough that template works
        ComponentInfo(component_ID = comp_id,
                      parents_serial = comp_sn,
                      pub_date = oldtime).save()
        response = self.client.get('/test_detail/%s/%s/' % (comp_id, "123"),
                                   follow = True)

        check = "Missing code/test"
        if check not in str(response.content):
            print(response.content)
        self.assertContains(response, check)

    def test_good_present(self):
        comp_id = '01234567890123456789012345678901'
        comp_sn = '20USWW01234567'
        # Enough that template works
        # Grab the model object of the component we're creating so we can associate the TestSummary to it
        # Separated into two lines so that we have a reference to the component for the test we 
        # create later, and then finally .save to local dbdb
        this_component = ComponentInfo(component_ID = comp_id,
                      parents_serial = comp_sn,
                      pub_date = oldtime)
        this_component.save()
        count = "123"
        test_data = {
            "results": [],
            "properties": [],
            "comments": [],
            "id": comp_id,
            "state": "ASD",
            "date": "2022",
            "passed": False,
            "components": [
                {"componentType":{"code": "ASD"},
                "code": comp_id}
            ],
        }
        TestSummary(component_ID = comp_id,
                    count = count,
                    test_content = test_data,
                    pub_date = oldtime,
                    associated_component=this_component).save()
        response = self.client.get('/test_detail/%s/%s/' % (comp_id, count),
                                   follow = True)

        check = "detail view"
        if check not in str(response.content):
            print(response.content)
        self.assertContains(response, check)
