from django.test import TestCase

import time
import urllib

from .test_db import PatchDBAccess

# Run CIDForm, but don't cause it to find a component
class CIDChecking(PatchDBAccess, TestCase):
    valid_sn = "20USXYY0123456"

    @classmethod
    def setUpTestData(cls):
        cls.try_again = [('CIDForm', 302)]

    def good_session(self):
        self.setup_session(time.time() + 10)

    def setup_session(self, e_t):
        s = self.client.session
        s["session_db_token"] = "our_token"
        s["session_db_expires_at"] = e_t
        s.save()

    def check_redirect(self, response, check, to_login = None):
        chain = self.try_again
        if to_login is not None:
            if to_login is True:
                chain = [('/login', 302)]
            else:
                chain = [(f'/login?{to_login}', 302)]

        self.assertEqual(chain, response.redirect_chain)

        if check not in str(response.content):
            print(response.content)
        self.assertContains(response, check)

    def test_bad_cid_form(self):
        response = self.client.post('/CIDForm', follow = True)
        self.check_redirect(response, "Invalid form data")

    # Expects token in session cookie
    def test_no_token_stored(self):
        response = self.client.post(
            '/CIDForm',
            {"without_request": False, "Component_ID": self.valid_sn},
            follow = True)
        redirect_to = urllib.parse.urlencode({"redirect": "/CIDForm"})
        self.check_redirect(response, "No authentication provided", redirect_to)

    def test_bad_length(self):
        self.good_session()
        d = {"without_request": False, "Component_ID": "0123"}
        response = self.client.post('/CIDForm', d, follow = True)
        self.check_redirect(response, "Bad component ID")

    def test_form_sn(self):
        self.good_session()
        d = {"without_request": False, "Component_ID": self.valid_sn}
        response = self.client.post('/CIDForm', d, follow = True)
        self.check_redirect(response, "Failed to get tests")

    def test_bad_length_cached(self):
        self.good_session()
        d = {"without_request": True, "Component_ID": "0123"}
        response = self.client.post('/CIDForm', d, follow = True)
        self.check_redirect(response, "length 14 serial number")

    def test_form_sn_cached(self):
        self.good_session()
        d = {"without_request": True, "Component_ID": self.valid_sn}
        response = self.client.post('/CIDForm', d, follow = True)
        self.check_redirect(response, f"No cached information on submitted component {self.valid_sn}")

    # Checks token in session cookie
    def test_stored_token_expired(self):
        # Something that's already expired
        self.setup_session(time.time() - 10)

        d = {"without_request": False, "Component_ID": self.valid_sn}
        response = self.client.post('/CIDForm', d, follow = True)
        redirect_to = urllib.parse.urlencode({"redirect": "/CIDForm"})
        self.check_redirect(response, "Authentication expired", redirect_to)

    # Checks token in session cookie
    def test_stored_token_good(self):
        # Will expire in a while
        self.setup_session(time.time() + 1000)

        d = {"without_request": False, "Component_ID": self.valid_sn}
        response = self.client.post('/CIDForm', d, follow = True)
        self.check_redirect(response, "Failed to get tests associated")

    def test_form_with_auth(self):
        response = self.client.get('/CIDForm')
        self.assertEqual(response.status_code, 200)
        # self.assertContains(response, "key1")

    def test_form_without_auth(self):
        self.setup_session(time.time() + 1000)
        response = self.client.get('/CIDForm')
        self.assertEqual(response.status_code, 200)
        # Auth only checked on post

    def test_form_with_auth_expired(self):
        self.setup_session(time.time() - 1000)
        response = self.client.get('/CIDForm')
        self.assertEqual(response.status_code, 200)
        # Auth only checked on post
