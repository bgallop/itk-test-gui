from django.test import TestCase
from .test_db import PatchDBAccess

from django.utils import timezone

import time
import datetime

from display_data.models import Institute

oldtime = timezone.now() - datetime.timedelta(days=1)

class InstTest(PatchDBAccess, TestCase):
    @classmethod
    def setUpTestData(cls):
        Institute(inst_name = "Long institute name",
                  inst_id = "0123456789",
                  inst_code = "INST_CODE",
                  pub_date = oldtime).save()

    def setup_session(self):
        s = self.client.session
        s["session_db_token"] = "our_token"
        # Expire in the future
        s["session_db_expires_at"] = time.time() + 1000
        s.save()

    # The one that calls the DB
    def test_fetch_inst(self):
        self.setup_session()

        inst_key = "listInstitutions: {}"

        # Only the properties we're interested in
        inst_info = [
            {
                "id": "987654321098765432109876",
                "code": "INST",
                "name": "Test Lab",
                "componentType": [
                    {
                        "code": "S",
                        "name": "Strips",
                        "itemList": [
                            {
                                "id": "012345678901234567890123",
                                "code": "MODULE",
                                "name": "Module"
                            }
                        ]
                    }
                ]
            },
            {
                "id": "98765432109876543210abcd",
                "code": "INST_2",
                "name": "Not strips Lab",
                "componentType": [
                    {
                        "code": "J",
                        "name": "NotStrips",
                        "itemList": [
                            {
                                "id": "01234567890123456789abcd",
                                "code": "MODULE",
                                "name": "Module"
                            }
                        ]
                    }
                ]
            }
        ]

        self.db_response[inst_key] = {"pageItemList": inst_info}
        response = self.client.post('/FetchInst', {"key1": "1", "key2": "2"}, follow = True)
        del self.db_response[inst_key]

        self.assertEqual([('listInst', 302)], response.redirect_chain)

        # Only one is stored (Strips)
        self.assertEqual(len(Institute.objects.all()), 1)

        for i in Institute.objects.all():
            self.assertEqual(i.inst_name, "Test Lab")

    def test_bad_fetch_inst(self):
        response = self.client.get('/FetchInst')
        self.assertEqual(response.status_code, 405)

    def test_bad_list_inst(self):
        response = self.client.post('/listInst')
        self.assertEqual(response.status_code, 405)

    def test_bad_inst_form(self):
        response = self.client.get('/inst/BAD_INST')
        self.assertRedirects(response, "/listInst")

    def test_inst_view(self):
        response = self.client.get('/inst/INST_CODE')
        self.assertEqual(response.status_code, 200)

    def test_inst_list(self):
        response = self.client.get('/listInst')
        self.assertEqual(response.status_code, 200)

    def test_inst_code_view(self):
        self.setup_session()

        list_key = "listComponents: {'filterMap': {'state': ['ready'], 'trashed': False, 'componentType': ['CT_CODE'], 'project': 'S', 'currentLocation': ['INST_CODE']}, 'outputType': 'object', 'pageInfo': {'pageIndex': 0, 'pageSize': 10}}"

        # One with SN, one without
        comp_info = {"itemList": [
            {'code': "12341234123412341234123412341234",
             'project': "S",
             'type': 'TYPE1'},
            {'code': "34123412341234123412341234123412",
             'project': "S",
             'type': 'TYPE2', 'serialNumber': '20USZZ01234567'},
        ]}

        self.db_response[list_key] = comp_info

        response = self.client.get('/inst/INST_CODE/CT_CODE', follow=True)
        del self.db_response[list_key]

        # Could do more testing here, eg of links (and missing_list etc)
        self.assertEqual(response.status_code, 200)
