import display_data.do_stuff as do_stuff

import logging

logger = logging.getLogger(__name__)

class PatchDBAccess:
    db_response = {None: {}}

    def setUp(self):
        self._save_get = do_stuff.do_db_get
        self._save_access = do_stuff.access

        def patch_db_get(comm, data, token):
            key = str("%s: %s" % (comm, data))
            logger.debug("dbGet: %s" % key)

            try:
                return self.db_response[key]
            except KeyError:
                return self.db_response[None]

        def patch_db_access(k1, k2):
            if k1 == "bad_auth1" and k2 == "bad_auth2":
                # Fail
                return True, None
            else:
                return False, "some_db_token"

        do_stuff.do_db_get = patch_db_get
        do_stuff.access = patch_db_access

    def tearDown(self):
        do_stuff.do_db_get = self._save_get
        do_stuff.access = self._save_access
