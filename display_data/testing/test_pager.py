from django.test import TestCase

from display_data.pager import VirtualPaginator

import logging

logger = logging.getLogger(__name__)

class TestVirtualPager(TestCase):
    def test_basic(self):
        vp = VirtualPaginator(10, 1000)

        self.assertEqual(vp.count, 1000)
        self.assertEqual(vp.per_page, 10)
        self.assertEqual(vp.num_pages, 100)

        logger.debug(f"Virtual page {vp!r}")
        page_num = 2
        self.assertEqual(vp.validate_number(page_num), page_num)

        # This returns what we normally pass to the template
        p = vp.get_page(page_num)
        logger.debug(f"Page {p!r}")

        self.assertEqual(p.has_previous(), True)
        self.assertEqual(p.previous_page_number(), 1)

        self.assertEqual(p.has_next(), True)
        self.assertEqual(p.next_page_number(), 3)

        # Not used, but could be
        self.assertEqual(p.start_index(), 11)
        self.assertEqual(p.end_index(), 20)
