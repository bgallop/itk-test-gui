from django.test import Client, TestCase

# Go through a view simple URLs and check they work
class BasicTest(TestCase):
    def test_index(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

    def test_plotting_works(self):
        c = Client()
        response = c.get('/test_plotting')
        self.assertEqual(response.status_code, 200)

    def test_empty_summary(self):
        c = Client()
        response = c.get('/summary')
        self.assertEqual(response.status_code, 200)

    def test_bad_index(self):
        c = Client()
        response = c.get('/asd')
        self.assertEqual(response.status_code, 404)

    def test_comp_form(self):
        response = self.client.get('/CIDForm')
        self.assertEqual(response.status_code, 200)
