from django.test import TestCase

from display_data.forms import ListFilterForm

class FilterTest(TestCase):
    def test_basic(self):
        """
        Test filter Form as it takes choices on construction.
        """

        f = ListFilterForm(choices = {"comp_pick": [("ANY", "any")]})
        self.assertEqual(f.errors, {})

        f = ListFilterForm(
            data = {"comp_pick": "ANY"},
            choices = {"comp_pick": [("ANY", "any")]},
        )
        self.assertEqual(f.errors, {})
        self.assertEqual(f.cleaned_data.get("comp_pick"), "ANY")

        f = ListFilterForm(
            initial = {"comp_pick": "ANY"},
            choices = {"comp_pick": [("ANY", "any")]},
        )
        self.assertEqual(f.errors, {})
        self.assertEqual(f["comp_pick"].initial, "ANY")
