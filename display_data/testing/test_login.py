from django.test import TestCase

import time

from .test_db import PatchDBAccess

class LoginTest(PatchDBAccess, TestCase):
    @classmethod
    def setUpTestData(cls):
        pass

    def setup_session(self, e_t):
        s = self.client.session
        s["session_db_token"] = "our_token"
        s["session_db_expires_at"] = e_t
        s.save()

    def is_try_again(self, response, check):
        # self.assertEqual(try_again, response.redirect_chain)

        if check not in str(response.content):
            print(response.content)
        self.assertContains(response, check)

    def test_login_get(self):
        """Calling login with GET displays login page"""

        response = self.client.get('/login')

        self.assertContains(response, "access key 1")

    def test_login_get_with_session(self):
        self.setup_session(time.time() + 1000)

        response = self.client.get('/login')

        # Valid token, go back to index
        self.assertRedirects(response, "/")

    def test_login_get_with_expired(self):
        self.setup_session(time.time() - 1000)

        response = self.client.get('/login')

        self.assertContains(response, "access key 1")

    def test_login_invalid(self):
        response = self.client.post('/login', follow = True)

        self.is_try_again(response, "Invalid form data")

    def test_login_good(self):
        """Test that successful login works (defaul redirect to /)"""

        d = {"key1": "1", "key2": "2"}
        response = self.client.post('/login', d, follow = True)

        self.is_try_again(response, "Login successful")

        s = self.client.session
        self.assertEqual(s["session_db_token"], "some_db_token")
        s["session_db_expires_at"]

    def test_login_good_redirect(self):
        """Test that successful login redirects afterwards"""

        d = {"key1": "1", "key2": "2"}
        d["redirect"] = "/summary"
        response = self.client.post('/login', d, follow = True)
        self.assertRedirects(response, "/summary")

        self.is_try_again(response, "Login successful")

        s = self.client.session
        self.assertEqual(s["session_db_token"], "some_db_token")
        s["session_db_expires_at"]


    def test_login_bad(self):
        d = {"key1": "bad_auth1", "key2": "bad_auth2"}
        response = self.client.post('/login', d, follow = True)

        self.is_try_again(response, "Authentication failed")

        s = self.client.session
        with self.assertRaises(KeyError) as _:
            s["session_db_token"]
        with self.assertRaises(KeyError) as _:
            s["session_db_expires_at"]

    def test_login_bad_clears(self):
        # Start with good token
        self.setup_session(time.time() + 1000)

        # Before post, these are valid
        self.client.session["session_db_token"]
        self.client.session["session_db_expires_at"]

        d = {"key1": "bad_auth1", "key2": "bad_auth2"}

        response = self.client.post('/login', d, follow = True)

        self.is_try_again(response, "Authentication failed")

        s = self.client.session
        with self.assertRaises(KeyError) as _:
            s["session_db_token"]
        with self.assertRaises(KeyError) as _:
            s["session_db_expires_at"]

