from django.test import TestCase
from django.utils import timezone

import datetime

import display_data.do_stuff as do_stuff
from display_data.models import ComponentInfo, TestSummary

from .test_db import PatchDBAccess

oldtime = timezone.now() - datetime.timedelta(days=1)

class TestSummaryModel(PatchDBAccess, TestCase):
    def test_creation(self):
        """
        This test originally was to test querying the ITK db for a TestRun, and then saving to the local DB, without having a valid ComponentInfo object to be associated to.
        It would then save that TestRun, and then check/assert that the TestRun exists, but not the associated ComponentInfo, which would appear to be unphysical.

        TODO: Clear up this test a bit, outline the goal
        """
        db_key = "getTestRun: {'testRun': '1234'}"

        self.db_response[db_key] = {
            "components": [
                {
                    "serialNumber": "20XXYY",
                    "testedAtStage": {"name": "ABC"},
                    "componentType": {"name": "CT"},
                    "type": {"name": "T"},
                }
            ],
            "testType": {"name": "TEST_TYPE"},
            "passed": True,
        }
        # t is standing in for one of the dicts that you get if you query the API for a component
        # and get component_api_response, and then look up the elements of the list 
        # component_api_response["tests"]
        t = {"testRuns": [
            {"id": "1234", "state": "ready"}
        ]}

        componentID = "123"
        # Create a mock component so that the test can look up something with this ID
        this_component = ComponentInfo(component_ID = componentID,
                      parents_serial = None,
                      pub_date = oldtime)
        this_component.save()

        # Note: the do_db_get in do_stuff that is being called by do_stuff has been overriden
        do_stuff.get_test(t, componentID, child=False, child_layer=0, child_number=0, token=None, test_count=0)

        cis = ComponentInfo.objects.all()
        self.assertEqual(len(cis), 1) # Previously asserting 0

        tms = TestSummary.objects.all()
        self.assertEqual(len(tms), 1)
        self.assertEqual(type(tms[0].test_content), dict)

