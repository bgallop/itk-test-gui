from django.test import TestCase

from display_data.models import ComponentInfo, TestSummary

import random

# Things we're testing
from display_data.do_stuff import store_component_in_model, store_test_data

def make_some_id(base = ''):
    target_len = 32
    rand_len = target_len - len(base)
    rand_str = ''.join(random.choice("abcdef0123456789") for _ in range(rand_len))
    return base + rand_str

def make_component_id():
    return make_some_id('1111')

def make_test_run_id():
    return make_some_id('2222')

def make_component_json(component_id):
    return {
        "id": component_id,
        "serialNumber": None,
        "parents": [],
        "componentType": {"name": "CT"},
        "type": {"name": "ABC"},
        "children": [],
        "code": component_id,
    }

def make_test_run_json(component_id):
    return {
        "components": [
            {
                "serialNumber": "20XXYY",
                "testedAtStage": {"name": "ABC"},
                "componentType": {"name": "CT"},
                "type": {"name": "T"},
                "id": component_id
            }
        ],
        "testType": {"name": "TEST_TYPE"},
        "passed": True,
    }

class StoreTestDataTest(TestCase):
    def test_store_test_data(self):
        """Test store test data"""

        # Check everything is empty to start
        self.assertIs(len(TestSummary.objects.all()), 0)
        self.assertIs(len(ComponentInfo.objects.all()), 0)

        component_id = make_component_id()
        test_run_id = make_test_run_id()

        comp_json = make_component_json(component_id)

        # Needs reference to component
        store_component_in_model(component_id, comp_json)

        self.assertIs(len(ComponentInfo.objects.all()), 1)

        test_json = make_test_run_json(component_id)
        store_test_data(test_json, component_id, test_run_id,
                        False, 0, 0, 0)

        self.assertIs(len(TestSummary.objects.all()), 1)

class StoreComponentData(TestCase):
    def test_store_component(self):
        """Test store single component"""

        # Check everything is empty to start
        self.assertIs(len(ComponentInfo.objects.all()), 0)

        component_id = make_component_id()
        comp_json = make_component_json(component_id)

        store_component_in_model(component_id, comp_json)

        self.assertEqual(len(ComponentInfo.objects.all()), 1)

        for c in ComponentInfo.objects.all():
            self.assertEqual(c.component_ID, component_id)
            self.assertEqual(getattr(c, "component_ID"), component_id)

    def test_store_full_component_list(self):
        """Test schema from listComponent (with object flag)

        Schema is different to getComponent, and also might change slightly
        for different components. so this is an example.

        NB I've converted to python and removed some (currently?) unused names
        """

        list_comp_json = {
            "project": "S",
            "subproject": "SB",
            "institution": "59f1e7bfdf054600056d35a8",
            "componentType": "66d9c7590317320043212e67",
            "type": "G-ACW",
            "properties": [
                {
                    "code": "VENDOR",
                    "value": "asd",
                }
            ],
            "comments": [],
            "attachments": [],
            "locations": [],
            "state": "ready",
            "stages": [
                {
                    "code": "VENDOR_RECEPTION",
                    "comment": None,
                }
            ],
            "currentStage": "VENDOR_RECEPTION",
            "completed": False,
            "assembled": False,
            "inventory": None,
            "alternativeIdentifier": None,
            "serialNumber": "20USBXT0000001",
            "shipmentDestination": None,
            "shipmentDestinationTs": None,
            "code": "0e1d6873b03fa9a1337c267b0e99a42e",
            "currentLocation": "59f1e7bfdf054600056d35a8",
            "reusable": False,
            "completedReason": None,
            "reworkedReason": None,
            "flags": [],
            "id": "66d9cd3c0317320043214562",
        }

        modelled, created, json_out = store_component_in_model(list_comp_json["code"], list_comp_json)
        self.assertEqual(modelled.serialNumber, "20USBXT0000001")
        # It wasn't there before
        self.assertEqual(created, True)
        self.assertEqual(json_out, list_comp_json)

    def test_store_full_get_component(self):
        """Test schema from getComponent

        Schema is different for different API calls, so this is an example.

        NB I've converted to python and removed some (currently?) unused names
        """

        get_comp_json = {
            "id": "5b3613f4057985000553a581",
            "code": "f3dc1a8ff41239fc5135d0cae4e528c1",
            "serialNumber": "20USE000000005",
            "alternativeIdentifier": None,
            "project": {"code": "S", "name": "Strips"},
            "subproject": {"code": "SE", "name": "Strip endcaps"},
            "institution": {
                "id": "5ad8627909d7a800067a4e72",
            },
            "currentLocation": {
                "id": "5ad8627909d7a800067a4e72",
            },
            "shipmentDestination": None,
            "componentType": {
                "id": "59c8bf70704320000525556b",
                "code": "BT",
                "name": "Bus Tape",
                "snAutomatically": False,
                "category": "item",
                "state": "active",
                "stages": [
                    {
                        "code": "BARE",
                        "name": "Bare tape",
                        "order": 1,
                        "initial": True,
                        "final": False,
                        "alternative": False,
                        "testTypes": [
                            {
                                "testType": "5eff0e2fd6f81a000ae0b303",
                                "nextStage": False,
                                "order": 0,
                            },
                            {
                                "testType": "5d31cbb0df98f20009739cd5",
                                "nextStage": False,
                                "order": 1,
                            },
                        ],
                    },
                ],
            },
            "type": {"code": "TAPE_FRONT", "name": None},
            "currentStage": {
                "code": "BARE",
                "name": "Bare tape",
                "order": 1,
                "initial": True,
                "final": False,
                "testTypes": [
                    {
                        "testType": "5eff0e2fd6f81a000ae0b303",
                        "nextStage": False,
                        "order": 0,
                    },
                ],
            },
            "properties": [
                {
                    "code": "PREPREG_ID",
                    "name": "Prepreg Material Roll ID",
                    "dataType": "string",
                },
            ],
            "children": [
                {
                    "id": "5b3613f4057985000553a582",
                    "order": None,
                    "componentType": {
                        "id": "59d13f7dbbcf12000502ba41",
                        "code": "UV_GLUE",
                        "name": "ASIC-H Glue (Obsolete)",
                        "category": "bulk",
                    },
                    "type": None,
                    "component": None,
                },
                {
                    "id": "5b3613f4057985000553a583",
                    "order": None,
                    "componentType": {
                        "id": "59d13fb3bbcf12000502ba42",
                        "code": "CU_POLYIMIDE",
                        "name": "Cu/polyimide",
                        "category": "bulk",
                    },
                    "type": None,
                    "component": None,
                },
            ],
            "parents": [],
            "flags": [],
            "stages": [
                {
                    "code": "BARE",
                    "name": "Bare tape",
                }
            ],
            "locations": [],
            "assembled": None,
            "grades": [],
            "batches": [],
            "comments": [],
            "tests": [],
            "attachments": [],
        }

        modelled, created, json_out = store_component_in_model(get_comp_json["code"], get_comp_json)
        self.assertEqual(modelled.serialNumber, "20USE000000005")
        # It wasn't there before
        self.assertEqual(created, True)
        self.assertEqual(json_out, get_comp_json)
