import base64
from io import BytesIO
import matplotlib.pyplot as plt
import numpy as np
import logging

logger = logging.getLogger(__name__)

plt.rcParams['figure.dpi'] = 200
plt.rcParams["legend.frameon"] = 0
plt.rcParams.update({'font.size': 8})

def get_graph():
    buffer = BytesIO()
    plt.savefig(buffer, format='png', dpi=300)
    buffer.seek(0)
    image_png = buffer.getvalue()
    graph = base64.b64encode(image_png)
    graph = graph.decode('utf-8')
    buffer.close()
    return graph

def get_plot(x,y, xlabel='', ylabel='', figsize=(5, 3), markersize=4):
    logger.debug("get_plot")
    plt.switch_backend('AGG')
    plt.figure(figsize=figsize)
    plt.plot(x, y, '.', color='black', markersize=markersize)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.tight_layout()
    graph = get_graph()
    logger.debug("get_plot complete")
    return graph

def get_plot_2(y0, y1, legend='STREAM0', legend2='STREAM1', xlabel='ABC chip', ylabel='', figsize=(5, 3)):
    logger.debug("get_plot_2")
    plt.switch_backend('AGG')
    plt.figure(figsize=figsize)
    x0 = np.arange(len(y0)) + 1
    x1 = np.arange(len(y1)) + 1
    plt.plot(x0, y0, marker='.', color='black', label=f'{legend}')
    plt.plot(x1, y1, marker='.', color='blueviolet', label=f'{legend2}')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.tight_layout()
    plt.legend(loc='upper right')
    graph = get_graph()
    logger.debug("get_plot_2 complete")
    return graph

def get_plot_errorbar(y0, y1, y0_err, y1_err, legend='STREAM0', legend2='STREAM1', xlabel='ABC chip', ylabel='', figsize=(5, 3)):
    logger.debug("get_plot_errorbar")
    plt.switch_backend('AGG')
    plt.figure(figsize=figsize)
    x0 = np.arange(len(y0)) + 1
    x1 = np.arange(len(y1)) + 1
    plt.errorbar(x0, y0, yerr=y0_err, marker='.', color='black', elinewidth=0.5, label=f'{legend}')
    plt.errorbar(x1, y1, yerr=y1_err, marker='.', color='blueviolet', elinewidth=0.5, label=f'{legend2}')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.tight_layout()
    plt.legend(loc='upper right')
    graph = get_graph()
    logger.debug("get_plot_errorbar complete")
    return graph

def get_plot_scatter(x,y,z, figsize=(5, 3), clabel='', xlabel='x [mm]', ylabel='y [mm]'):
    logger.debug("get_plot_scatter")
    plt.switch_backend('AGG')
    plt.figure(figsize=figsize)
    plt.scatter(x, y, c=z, s=5, cmap='Blues')
    plt.colorbar(label=clabel)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.tight_layout()
    graph = get_graph()
    logger.debug("get_plot_scatter complete")
    return graph
