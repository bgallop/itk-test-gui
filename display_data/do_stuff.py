import pprint # Mostly used for debug printing

from display_data.models import ComponentInfo, ComponentData, Institute, StatsCounter, TestSummary
import display_data.dbAccess as dbAccess
import json
from display_data.specific_stuff import interesting_top_level_codes

from django.utils import timezone
from django.conf import settings
import django.core.exceptions

import logging

logger = logging.getLogger(__name__)

def delete_tests():
    test_list = TestSummary.objects.filter()
    for test in test_list:
        if not test.was_published_recently():
            pk = test.pk
            TestSummary.objects.filter(pk=pk).delete()

def delete_components():
    test_list = ComponentInfo.objects.filter()
    for test in test_list:
        if not test.was_published_recently():
            pk = test.pk
            ComponentInfo.objects.filter(pk=pk).delete()

def do_db_get(action, data, token):
    logger.debug("DB Get {}".format(action))
    count_obj, _ = StatsCounter.objects.update_or_create(stat_name=action)
    count_obj.stat_counter += 1
    count_obj.save()

    out_data = dbAccess.doSomething(action, method='get',
                                    data=data, token=token)

    return out_data

def store_test_data(test_json, componentID, testRunID, child=False, child_layer=0, child_number=0, test_count=0):
    """
    Store test data in local database (TestSummary)

    Args:
        test_json - Json data on test
        componentID - Component ID of the (top-level) component this test is associated to
        child - Is this a child (default False)
        child_layer - How far down in the hierarchy (default 0)
        child_number - Index of child in parent object (default 0)
        test_count - Index of test within top-level parent (default 0)
    """

    test_component = test_json['components'][0]
    
    if "serialNumber" not in test_component:
        serial = None
    else:
        serial = test_component['serialNumber']

    if serial is None:
        serialNumber = 'No ATLAS serial number'
    else:
        serialNumber = serial

    try:
        stage = test_component['testedAtStage']['name']
    except KeyError:
        stage = None
    if stage is None:
        try:
            stage = test_component['testedAtStage']['code']
        except KeyError:
            stage = None
    if stage is None:
        stage = '-'

    test_passed = test_json['passed']
    if test_passed is None:
        test_passed = False

    Test = TestSummary(
        test_name=test_json['testType']['name'],
        test_id=testRunID,
        test_content=test_json,
        passed=test_passed,
        component_type=test_component['componentType']['name'],
        type=test_component['type']['name'],
        serialNumber=serialNumber,
        component_ID=componentID,
        child=child,
        child_layer=child_layer,
        child_number=child_number,
        testedAtStage=stage,
        pub_date=timezone.now(),
        count=test_count,
        associated_component = ComponentInfo.objects.get(component_ID=componentID)
    )

    Test.save()
    test_count = test_count + 1
    return test_count

def get_test(t, componentID, child=False, child_layer=0, child_number=0, token=None, test_count=0):
    """
    Looks up a test on the ITK database, saves to local database

    Args:
        t - A dict, of the form of those generated by the ITK DB API when looking up a component and under the key "tests"
        componentID - Component ID of the component this test is associated to
        child - Is this a child (default False)
        child_layer - How far down in the hierarchy (default 0)
        child_number - Index of child in parent object (default 0)
        token - Authentication token for ITK DB, default None
        test_count - Index of test within top-level parent (default 0)
    """
    index = 0
    flag=True
    for k in range(0, len(t['testRuns'])):
        if t['testRuns'][k]['state'] == 'ready' and flag is True:  # exclude deleted tests
            index = k
            flag = False
    testRunID = t['testRuns'][index]['id']

    test_output = get_test_single(testRunID, token)

    return store_test_data(test_output, componentID, testRunID, child, child_layer, child_number, test_count)

def access(key1, key2):
    failed, token = dbAccess.setupConnection(accessCode1=key1, accessCode2=key2)
    return failed, token

def upload_test_result(token, filename, componentID, institution='HUBERLIN', comments=''):
    d = filename.read()
    data = json.loads(d)
    data["component"] = componentID
    data["institution"] = institution
    data["comments"] = [comments]

    # POST
    dbAccess.doSomething("uploadTestRunResults", data, token = token)


# get object later

# TestSummary.objects.all()
# T = TestSummary.objects.get(pk=1)
# T.test_content['passed']

def get_component(component_id, token):
    """
    Queries ITK production database for a single component, using DB component ID
    """
    command = "getComponent"
    extra_data = {
        "component" : component_id
    }
    component = do_db_get(command, extra_data, token)
    return component

def get_test_single(test_run_id, token):
    """
    Queries the ITK production database for a single component, using a DB component ID

    Runs for a single test, later problem is to batch these

    Unlike get_test, this does not automagically put into the DB
    """

    command = "getTestRun"
    extra_data = {
        "testRun" : test_run_id
    }

    test_run = do_db_get(command, extra_data, token)
    return test_run

def get_bulk_tests(test_run_ids, token):
    """
    Given a list of test run IDs and the access token, get the API response

    Using requests manually here
    """

    command = "getTestRunBulk"
    extra_data = {
        "testRun" : test_run_ids
    }

    test_runs = do_db_get(command, extra_data, token)
    return test_runs

def sync_tests_to_db(test_run_ids, token):
    """
    Given a list of test run IDs, sync these to the local db
    """
    import time
    start_time = time.time()
    test_runs_response = get_bulk_tests(test_run_ids, token)
    end_time = time.time()
    print(f"Run time is {end_time - start_time}")
    for i, test in enumerate(test_runs_response["itemList"]):
        # TODO: Figure out how this is meant to interact with the other tests?
        # Why is components a list?
        assoc_component = ComponentInfo.objects.get(
            component_ID=test["components"][0]["code"]
        )
        update_dict = {
            "test_name" : test["testType"]["name"],
            "passed" : test["passed"],
            "test_content" : test,
            "type" : test["testType"]["code"],
            "testedAtStage" : test["components"][0]["testedAtStage"]["code"],
            "associated_component" : assoc_component,
            "pub_date" : timezone.now(), # ? Need to check if Django is set to be timezone aware, it seems to be according to settings
        }
        try:
            test_run_summary, created = TestSummary.objects.update_or_create(
                test_id=test["id"],
                defaults=update_dict
            )
        except Exception as error:
            # Some debug code, can be removed at later date
            print(f"Issue when saving a test {i} of {len(test_runs_response['itemList'])}")
            print("update dict is:")
            pprint.pprint(update_dict)
            raise error
    # Next step is to go through each test in the response and serialise appropriately
    # At this stage, afaik, the biggest issue is whether they passed or not
    return 

def get_tests_by_component(component_id, token):
    """
    Queries the ITK DB for which tests belong to a component
    """
    command = "listTestRunsByComponent"
    extra_data = {
        "component" : component_id,
    }
    tests_by_component = do_db_get(command, extra_data, token)

    return tests_by_component

def get_test_ids_for_component_tree(component_tree, token):
    """
    For a given component tree, recursively get associated test IDs to pull via bulk query

    Calls ITK DB via get_tests_by_component
    
    Args:
        component_tree : Nested dicts/tree of components, created by function in views.defect_tree_view
        token : db access token
    """

    
    # First, get a list of component IDs from the tree
    def recursive_list(component_tree):
        result = []
        result.append(component_tree["component_id"])
        for child in component_tree["children"]:
            child_ids = recursive_list(child)
            result = result + child_ids
        return result
    component_ids_to_query = recursive_list(component_tree)

    print(f"List of component IDs to query for tests: {component_ids_to_query}")
    print(f"There are {len(component_ids_to_query)} ids to query")

    # Now that we have a list of components, lets query for associated test IDs
    # The bulk route doesn't appear to query for ALL the info, but a bit, but we'll use it for now
    test_run_ids = []
    for component_id in component_ids_to_query:
        response = get_tests_by_component(component_id, token)
        these_test_run_ids = [test_run["id"] for test_run in response["pageItemList"]]
        test_run_ids = test_run_ids + these_test_run_ids
    return test_run_ids


def store_component_from_db_in_model(component_id, token):
    """
    Function too look up component ID, and store in local DB

    Args:
        component_id : Component ID used to look up object on ITK DB. Not the ATLAS SN, not one of the Mongo IDs, but a guaranteed (UUID4?) unique number representing a physical module
        token : Token used to access DB

    Returns:
        component : The Django object returned by create or update
        created : Whether a new object was created
        api_response : The raw API response for the object queried for, as a dict

    """
    api_response = get_component(component_id, token)

    return store_component_in_model(component_id, api_response)


def store_component_in_model(component_id, json_data):
    """
    Store component data in local DB

    Args:
        component_id : Component ID used to look up object on ITK DB. Not the ATLAS SN, not one of the Mongo IDs, but a guaranteed (UUID4?) unique number representing a physical module
        json_data : Test data from database to be stored

    Returns:
        component : The Django object returned by create or update
        created : Whether a new object was created
        api_response : The raw API response for the object queried for, as a dict

    """

    api_response = json_data

    # Look up some values to be used for later insert
    serial_number = None
    if "serialNumber" in api_response:
        if api_response["serialNumber"] is not None:
            serial_number = api_response["serialNumber"]

    alt_id = None
    if "alternativeIdentifier" in api_response:
        alt_id = api_response["alternativeIdentifier"]

    curr_stage = None
    if "currentStage" in api_response and api_response["currentStage"] is not None and "name" in api_response["currentStage"]:
        curr_stage = api_response["currentStage"]["name"]

    batch_num = None
    if "batches" in api_response:
        if len(api_response["batches"]) > 0 and "number" in api_response["batches"][0]:
            batch_num = api_response["batches"][0]["number"]

    # Work out parental relatinships
    parents = api_response['parents'] if "parents" in api_response else None
    
    if parents:
        parents_with_component = [i for i in parents if i['component']]

        # Use the parents with comnponent set, otherwise we're looking at old parents the 'thing; is detached from

        if len(parents_with_component) > 0:
            my_parent = parents_with_component[0]
            if "component" in my_parent and "code" in my_parent["component"]:
                # Initially assuming one parent slot with component set
                parents_id = my_parent["component"]["code"]
                parents_type = my_parent['componentType']['name']
                parent_serial = my_parent['component']['serialNumber']
            else:
                # from listComponents:object
                parents_id = my_parent["component"]
                # Actually parent code
                parents_type = my_parent['componentType']
                parent_serial = None # my_parent['component']['serialNumber']
        elif parents[0]['history'][0]:
            try:
                # History represents old parents that were connected
                print("Select via history")
                parents_id = parents[0]['history'][0]['component']['code']
                parents_type = parents[0]['history'][0]['componentType']['name'] + ' (history, no current parent component)'
                parent_serial = parents[0]['history'][0]['component']['serialNumber']
            except TypeError:
                # Could extract the same, but not sure it's useful
                parents_id = None
                parent_serial = '-'
                parents_type = None
    else:
        parents_id = None
        parent_serial = '-'
        parents_type = None
    if parent_serial is None:
        parent_serial = '-'
        parents_type = None

    # If we're starting from the top, we may not have the parent, so deal with object not found
    try:
        parent_component_info = ComponentInfo.objects.get(component_ID=parents_id)
    except django.core.exceptions.ObjectDoesNotExist:
        parent_component_info = None

    if api_response["componentType"] is None:
        component_type = "Unknown"
        logger.error(f"Component type is unknown for {component_id}")
    else:
        ct = api_response["componentType"]
        if "name" in ct:
            # getComponent
            component_type = ct["name"]
        else:
            # listComponents:object (not the name)
            component_type = ct

    if api_response["type"] is None:
        sub_type = "Unknown"
        logger.error(f"Component subtype is unknown for {component_id}")
    else:
        if "name" in api_response["type"]:
            sub_type = api_response["type"]["name"]
            if sub_type is None:
                sub_type = ''
        else:
            sub_type = api_response["type"]

    update_dict = {
        "component_type" : component_type,
        "type" : sub_type,
        "serialNumber" : serial_number,
        "parents_ID" : parents_id,
        "parents_type" : parents_type,
        "parents_serial" : parent_serial,
        "pub_date" : timezone.now(),
        "nChildren" : len(api_response["children"]) if ("children" in api_response and api_response["children"]) else 0,
        "parent_component" : parent_component_info,
        "alternative_ID": alt_id,
        "batch_name": batch_num,
        "current_stage": curr_stage,
    }

    # Lookup based on component ID, create new values. 
    # Defaults are the values set if the object already exists
    try:
        component, created = ComponentInfo.objects.update_or_create(
            component_ID=api_response["code"],
            defaults=update_dict
        )
    except Exception as error:
        # Some debug code, can be removed at later date
        print("Issue when saving")
        print("update dict is:")
        import pprint
        pprint.pprint(update_dict)
        raise error
    return component, created, api_response

def store_test(component_id, test_id, token):
    """
    Given a test run id, pull from DB using the token and link to the indicated component ID
    """

    # TODO: In progress, finish this
    api_response = get_test_single(test_id, token)

    update_dict = {

    }
    # Somewhat similar to the other test function, but we'll just do one
    # For the component we use the 'code' as pk, not the id, but here we'll have to use the id to lookup
    # Best practice is not to align IDs but we need them to look up stuff, should be okay because of guaranteed uniqueness
    test_run, created = TestSummary.objects.update_or_create(
        test_id=api_response["id"],
        defaults=update_dict
    )
    return 

def store_component_and_children_in_db(component_id, token):
    """
    Recurses through all the children, and stores in DB
    Uses the function store_component_in_db to store each component, one at a time

    Args:
        component_id : Component ID used to look up object on ITK DB. Not the ATLAS SN, not one of the Mongo IDs, but a guaranteed (UUID4?) unique number representing a physical module
        token : Token used to access DB

    Returns:
        component : The Django object returned by create or update
        created : Whether a new object was created
        api_response : The raw API response for the top level object queried for, as a dict

    """
    component, created, api_response = store_component_from_db_in_model(component_id, token)
    if api_response["children"] is not None:
        for child in api_response["children"]:
            if child["component"] is not None:
                child_component_id = child["component"]["code"]
                store_component_and_children_in_db(child_component_id, token)

    return component, created, api_response

def get_associated_tests(componentID, token):
    """
    Get associated tests does a bunch of stuff. Given a componentID and token it:
    queries for the component
    Deletes all cached components and tests associated to this ID
    Creates a new component object
    Queries each associated test on the DB
    Queries each child on the DB
    Queries each child test on the DB
    """

    logger.debug(f"get_associated_tests: {componentID}")

    test_count = 0
    # get output for module
    Out = get_component(componentID, token)
    componentID = Out['code']  # redefine component ID in case ATLAS serial number was given
    serial = Out['serialNumber']
    if serial is None:
        serialNumber = 'No ATLAS serial number'
    else:
        serialNumber = serial

    # clear database entries matching requested component ID:
    ComponentInfo.objects.filter(component_ID=componentID).delete()
    TestSummary.objects.filter(component_ID=componentID).delete()

    logger.debug("get_associated_tests: Touch cache")
    delete_tests()  # delete tests that are older than 24h
    delete_components()  # delete components that are older than 24h

    parents = Out['parents']
    if parents:
        if parents[0]['component']:
            parents_ID = parents[0]['component']['code']
            parents_type = parents[0]['componentType']['name']
            parent_serial = parents[0]['component']['serialNumber']
        elif parents[0]['history'][0]:
            parents_ID = parents[0]['history'][0]['component']['code']
            parents_type = parents[0]['history'][0]['componentType']['name'] + ' (history, no current parent component)'
            parent_serial = parents[0]['history'][0]['component']['serialNumber']
    else:
        parents_ID = '-'
        parents_type = 'No parent components'
        parent_serial = '-'
    
    if parent_serial is None:
        parent_serial = '-'

    tests = Out['tests']
    children = Out['children']
    nChildren = 0
    if children:
        nChildren = len(children)
    else:
        children = []

    comp_sub_type = Out['type']['name'] if "type" in Out else ''
    if comp_sub_type is None:
        comp_sub_type = ''
    Component = ComponentInfo(component_type=Out['componentType']['name'],
                              type=comp_sub_type,
                              component_ID=Out['code'],
                              serialNumber=serialNumber,
                              parents_ID=parents_ID,
                              parents_type=parents_type,
                              parents_serial=parent_serial,
                              pub_date=timezone.now(),
                              nChildren=nChildren)
    Component.save()


    for t in tests:
        logger.debug(f"get_associated_tests: Fetch test info {t}")
        test_count = get_test(t, componentID, child=False, child_layer=0, child_number=0, token=token, test_count=test_count)

    # loop over child components and extract associated tests
    child_number = 1

    for child in children:
        logger.debug("get_associated_tests: Check child")

        if child['component'] is not None:
            child_componentID = child['component']['code']
            child_Out = get_component(child_componentID, token)
            try:
                child_tests = child_Out['tests']
            except KeyError:
                continue

            for t in child_tests:
                logger.debug(f"get_associated_tests: Check child test {t}")
                test_count = get_test(t, componentID, child=True, child_layer=1, child_number=child_number, token=token, test_count=test_count)

        child_number = child_number + 1


    return componentID

# Add institute based on info from component
def add_inst_info(comp_info):
    time = timezone.now()

    inst = Institute(inst_id = comp_info["id"],
                     inst_code = comp_info["code"],
                     inst_name = comp_info["name"],
                     pub_date = time)

    inst.save()

def list_institutes(token, clear_first = True):
    command = "listInstitutions"

    inst_output = do_db_get(command, {}, token)

    try:
        inst_list = inst_output["pageItemList"]
    except KeyError:
        inst_list = inst_output["itemList"]

    if clear_first:
        print("Deleted all institutes first")
        Institute.objects.all().delete()

    for inst in inst_list:
        # Name changes (to componentTypes) on Ruby->JS migration
        ct_attr = "componentTypes"
        if ct_attr not in inst:
            ct_attr = "componentType"
            if ct_attr not in inst:
                continue

        strip_module_inst = False

        for ct in inst[ct_attr]:
            if "code" not in ct:
                continue
            if ct["code"] != "S":
                continue
            if ct["name"] != "Strips":
                continue

            for cti in ct["itemList"]:
                if cti["code"] in interesting_top_level_codes:
                    strip_module_inst = True

        if not strip_module_inst:
            continue

        add_inst_info(inst)

def list_institute_components(token, inst_code, ct_code):
    command = "listComponents"
    data = {
        "filterMap": {
            "state": ["ready"],
            "trashed": False,
            "componentType": [ct_code],
            "project": "S",
            "currentLocation": [inst_code]
        },
        # Don't fetch too much
        "outputType": "object"
    }

    data["pageInfo"] = {"pageIndex": 0, "pageSize": 10}
    # print("%s"% data)
    comps_output = do_db_get(command, data, token)

    # For now, just keep the serial numbers
    #  could fill the Component list, but currently that means
    #  also downloading all the Tests...
    sns = []

    for component in comps_output["itemList"]:
        if component["project"] != "S":
            continue

        logger.debug(f"Check info {component.keys()}")

        if settings.DEBUG:
            ComponentData.objects.update_or_create(
                component_ID = component["code"],
                defaults = {
                    "list_component_object": component,
                    "pub_date": timezone.now(),
                })

        comp_info = {"code": component["code"],
                     "type": component["type"]}

        if "serialNumber" in component:
            comp_info["sn"] = component["serialNumber"]
        if "alternativeIdentifier" in component:
            comp_info["alt_id"] = component["alternativeIdentifier"]

        sns.append(comp_info)

    return sns

def list_components(token, page_index = 0, page_size = 10, **keys):
    """Fetch list of components according to parameters.

    @param page_index Page index (0-based).
    @param page_size Size of list to return.

    Return tuple of components in model and total count.
    """

    logger.debug(f"ListComponent Page input: {page_index} ({page_size})")
    command = "listComponents"
    data = {
        "filterMap": {
            "state": ["ready"],
            "trashed": False,
            "project": "S",
        },
        "pageInfo": {
            "pageSize": page_size,
            "pageIndex": page_index,
        },
        # Don't fetch too much information
        "outputType": "object"
    }

    if "comp_type" in keys:
        ct_code = keys["comp_type"]
        data["filterMap"]["componentType"] = [ct_code]

    if "curr_inst" in keys:
        inst_code = keys["curr_inst"]
        data["filterMap"]["currentLocation"] = [inst_code]

    if "sub_type" in keys:
        tt = keys["sub_type"]
        data["filterMap"]["type"] = [tt]

    if "stage" in keys:
        st = keys["stage"]
        data["filterMap"]["currentStage"] = [st]

    if "past_stage" in keys:
        st = keys["past_stage"]
        data["filterMap"]["stage"] = [st]

    comps_output = do_db_get(command, data, token)

    logger.debug(f"ListComponent Paging: {comps_output['pageInfo']}")

    comp_total = comps_output["pageInfo"]["total"]

    comp_list = []

    for component in comps_output["itemList"]:
        if component["project"] != "S":
            continue

        logger.debug(f"Store info {component.keys()}")

        ComponentData.objects.update_or_create(
            component_ID = component["code"],
            defaults = {
                "list_component_object": component,
                "pub_date": timezone.now(),
            })

        c_model = store_component_in_model(component["code"], component)[0]

        comp_list.append(c_model)

    return comp_list, comp_total
