"""
SW 2023-03-23

Quick implementation of module summary test 
"""

import datetime
import logging

from display_data import dbAccess

logger = logging.getLogger(__name__)

def module_test_func(module_sn, token):
    """
    Working out a test function for doing a module summary test

    Sometimes there's issue looking up child component tests by id? So that's why the component code has been used
    """
    # A quick global for me to look at various things in here
    global temp


    # First lookup module itself
    extra_data = {
        "component" : module_sn
    }

    logger.debug(f"module_summary_test(test_func): getComponent {module_sn}")

    component_api_resp = dbAccess.doSomething(
        "getComponent",
        method='GET',
        data=extra_data,
        token=token
    )

    # Basic return result
    module_summary_test_result = {
        "component" : "",
        "testType" : "module_summary_test",
        "date" : datetime.datetime.now(datetime.timezone.utc).isoformat(timespec="milliseconds"),
        "passed" : False,
        "comments" : [],
        "properties" : {"module_test_func_version" : "0.0", "module_test_git_hash" : "0"},
        "results" : {},
    }

    if len(component_api_resp["uuAppErrorMap"]) > 0:
        # Error present
        print("Issue with lookup")
        # print(component_api_resp)
        module_summary_test_result["lookup_success"] = False
        module_summary_test_result["lookup_fault"] = "uuAppErrorMap"
        return module_summary_test_result, component_api_resp
    else:
        pass
        # module_summary_test_result["lookup_success"] = True
    # Set "component" to component ID, in case a SN was used
    
    # Check a module was looked up
    module_type_codes = [
        "R0",
        "R1",
        "R2",
        "R3",
        "R4",
        "R5",
        "BARREL_SS_MODULE",
        "BARREL_LS_MODULE",
        # Half modules
        "R3M0_HALFMODULE",
        "R3M1_HALFMODULE",
        "R4M0_HALFMODULE",
        "R4M1_HALFMODULE",
        "R5M0_HALFMODULE",
        "R5M1_HALFMODULE",
    ]
    if component_api_resp["type"]["code"] not in module_type_codes:
        # Didn't look up a module
        module_summary_test_result["lookup_success"] = False
        module_summary_test_result["lookup_fault"] = "NOT_MODULE_TYPE"
        return module_summary_test_result, component_api_resp

    # At this point assume lookup was good, fill in later with more checking
    module_summary_test_result["lookup_success"] = True

    module_summary_test_result["component"] = component_api_resp["code"]

    # Check in finished state, or at loading site, or on petal
    half_modules = [
        "R3M0_HALFMODULE",
        "R3M1_HALFMODULE",
    ]
    if component_api_resp["type"]["code"] in half_modules:
        finished_stages = [
            "STITCH_BONDING",
        ]
        if component_api_resp["currentStage"]["code"] in finished_stages:
            module_summary_test_result["results"]["module_finished_assembly"] = True
        else:
            module_summary_test_result["results"]["module_finished_assembly"] = False
    else:
        finished_stages = [
            "FINISHED", "AT_LOADING_SITE", "ON_CORE",
        ]
        if component_api_resp["currentStage"]["code"] in finished_stages:
            module_summary_test_result["results"]["module_finished_assembly"] = True
        else:
            module_summary_test_result["results"]["module_finished_assembly"] = False

    # Attach module comments to module
    # But we only want the text, and potentially the code
    #TODO: Fix module summary test for split modules
    if component_api_resp["type"]["code"] in ["R3", "R4", "R5"]:
        # Run the merging of the split module results
        # To do this, we'll need to do a module report for the left and right submodules, and we'll need to grab high level tests
        
        # Step 0 - Get daughter module IDs
        top_module_api_response = component_api_resp
        if top_module_api_response["type"]["code"] == "R3":
            left_module = [i for i in top_module_api_response["children"] if i["type"]["code"] == "R3M0_HALFMODULE"][0]
            right_module = [i for i in top_module_api_response["children"] if i["type"]["code"] == "R3M1_HALFMODULE"][0]
        elif top_module_api_response["type"]["code"] == "R4":
            left_module = [i for i in top_module_api_response["children"] if i["type"]["code"] == "R4M0_HALFMODULE"][0]
            right_module = [i for i in top_module_api_response["children"] if i["type"]["code"] == "R4M1_HALFMODULE"][0]
        elif top_module_api_response["type"]["code"] == "R5":
            left_module = [i for i in top_module_api_response["children"] if i["type"]["code"] == "R5M0_HALFMODULE"][0]
            right_module = [i for i in top_module_api_response["children"] if i["type"]["code"] == "R5M1_HALFMODULE"][0]
        else:
            print("Unable to deduce module type")
            raise ValueError

        left_module_id = left_module["component"]["code"]
        right_module_id = right_module["component"]["code"]


        # Step 1 - Get submodule test results
        left_module_summary_test_result, left_component_api_resp = module_test_func(left_module_id, token)
        right_module_summary_test_result, right_component_api_resp = module_test_func(right_module_id, token)

        # Get tests at top level
        # First get test run IDs
        extra_data = {
            "component" : top_module_api_response["code"],
        }
        # Note that ITSDAQ electrical tests are assigend to the child hybrids for some reason

        logger.debug(f"module_summary_test (r3,r4,r5): listTestRunsByComponent {extra_data['component']}")

        response = dbAccess.doSomething(
            "listTestRunsByComponent",
            method='GET',
            data=extra_data,
            token=token
        )
        test_run_ids = [test_run["id"] for test_run in response["pageItemList"]]
        # Now look up the test runs 
        extra_data = {
            "testRun" : test_run_ids
        }

        logger.debug(f"module_summary_test (r3,r4,r5): getTestRunBulk {extra_data['testRun']}")

        response = dbAccess.doSomething(
            "getTestRunBulk",
            method='GET',
            data=extra_data,
            token=token
        )
        # Now group tests by type, and get only the most recent
        module_tests = response["itemList"] # A list of all tests associated to a module
        # Group tests by type
        tests_dict = {}
        for test in module_tests:
            test_type_code = test["testType"]["code"]
            if test_type_code not in tests_dict:
                tests_dict[test_type_code] = []
            tests_dict[test_type_code].append(test)
        # Now sort by date for each test
        for test_type_code, these_tests in tests_dict.items():
            # Sort each by test date, latest first
            tests_dict[test_type_code] = sorted(these_tests, key=lambda test: test["date"], reverse=True)
        top_module_tests_dict = tests_dict

        # Now we've gotten everything, run the merge (no further db accesses inside)
        module_summary_test_result = run_merge(module_summary_test_result, component_api_resp, top_module_tests_dict, left_module_summary_test_result, right_module_summary_test_result)

        # Finish
        return module_summary_test_result, component_api_resp

    if component_api_resp["comments"] is not None:
        comment_strings = [i["code"] + " - " + i["comment"] for i in component_api_resp["comments"]]
        module_summary_test_result["comments"] = module_summary_test_result["comments"] + \
            comment_strings
    else:
        module_summary_test_result["comments"] = []
    
    # A dict that gives the component codes that should be found attached to the 
    # module
    # Each element of dict contains the type listed by the slot and the component type name
    children_dict = {
        "R0" : [
            ["R0PB", "Powerboard"],
            ["R0H0", "STAR Hybrid Assembly"],
            ["R0H1", "STAR Hybrid Assembly"],
            ["ATLAS18R0", "Sensor"],
        ],
        "R1" : [
            ["R1PB", "Powerboard",],
            ["R1H0", "STAR Hybrid Assembly"],
            ["R1H1", "STAR Hybrid Assembly"], 
            ["ATLAS18R1", "Sensor"],
        ],
        "R2" : [
            ["R2PB", "Powerboard"], # R2 Powerboard
            ["R2H0", "STAR Hybrid Assembly"], # Star hybrid assembly, R2s only have a single hybrid?
            ["ATLAS18R2", "Sensor"], # ATLAS18R2 Sensor, production sensor
        ],
        "R3" : [
            "R3M0_HALFMODULE",
            "R3M1_HALFMODULE"
        ],
        "R4" : [
            "R4M0_HALFMODULE",
            "R4M1_HALFMODULE"
        ],
        "R5" : [
            "R5M0_HALFMODULE",
            "R5M1_HALFMODULE"
        ],
        "BARREL_SS_MODULE": [
            ["B3", "Powerboard"],
            ["X", "STAR Hybrid Assembly"],
            ["Y", "STAR Hybrid Assembly"],
            ["ATLAS18SS", "Sensor"],
        ],
        "BARREL_LS_MODULE": [
            ["B3", "Powerboard"],
            ["X", "STAR Hybrid Assembly"],
            ["ATLAS18LS", "Sensor"],
        ],
        "R3M0_HALFMODULE" : [
            ["R3H0", "STAR Hybrid Assembly"],
            ["R3H2", "STAR Hybrid Assembly"],
            ["R3PB", "Powerboard"],
            ["ATLAS18R3", "Sensor"],
        ],
        "R3M1_HALFMODULE" : [
            ["R3H1", "STAR Hybrid Assembly"],
            ["R3H3", "STAR Hybrid Assembly"],
            ["ATLAS18R3", "Sensor"],
        ],
        "R4M1_HALFMODULE" : [
            ["R4H1", "STAR Hybrid Assembly"],
            ["ATLAS18R4", "Sensor"],
        ],
        "R4M0_HALFMODULE" : [
            ["R4H0", "STAR Hybrid Assembly"],
            ["ATLAS18R4", "Sensor"],
            ["R45PB", "Powerboard"]
        ],
        "R5M1_HALFMODULE" : [
            ["R5H1", "STAR Hybrid Assembly"],
            ["ATLAS18R5", "Sensor"],
        ],
        "R5M0_HALFMODULE" : [
            ["R5H0", "STAR Hybrid Assembly"],
            ["ATLAS18R5", "Sensor"],
            ["R45PB", "Powerboard"]
        ],
        
    }
    # Gets the type codes of required child components
    component_codes = children_dict[component_api_resp["type"]["code"]]
    # Next, make sure all child components are attached
    # Looking at https://itkpd-test.unicorncollege.cz/componentTypeView?id=59d60c13ed67730005160cd8
    # to make sure we have all child comps
    found_dict = dict((component_code, False) for (component_code,_) in component_codes)
    # A dict keyed by required child component type and the code of the component actually in 
    # that slot
    child_component_code_dict = {}
    # Look for presence of required parts
    for child_type_code, component_type_name in component_codes:
        # Find children that match the child type and the component type
        # Some children might have missing fields, example https://itkpd-test.unicorncollege.cz/componentView?code=5e16e14e61a6587fc14491643dbf6910
        # with ATLAS serial number 20USEM00000028. Not sure how best to mitigate, but seems old 
        filtered_children = [i for i in component_api_resp["children"] if i["type"] is not None]
        slots = [
            i for i in filtered_children if i["type"]["code"] == child_type_code and i["componentType"]["name"] == component_type_name
        ]
        if len(slots) != 1:
            # Should only be selecting one slot
            print(f"Slots not equal to 1 (length {len(slots)}) for child type code {child_type_code}, component type {component_type_name}")
            raise ValueError
        this_component_slot = slots[0]
        # There should be only one slot
        if this_component_slot["component"] is not None:
            # If there's a component in the slot, it's present
            found_dict[child_type_code] = True
            # Get code to look up stage
            temp = this_component_slot
            child_component_code_dict[child_type_code] = this_component_slot["component"]["code"]



    # Let's look at the tests associated to module, start at whole module level
    extra_data = {
        "component" : component_api_resp["code"],
    }
    # Note that ITSDAQ electrical tests are assigend to the child hybrids for some reason

    logger.debug(f"module_summary_test (any): listTestRunsByComponent {extra_data['component']}")

    response = dbAccess.doSomething(
        "listTestRunsByComponent",
        method='GET',
        data=extra_data,
        token=token
    )
    test_run_ids = [test_run["id"] for test_run in response["pageItemList"]]
    # Also, look up the hybrid code and get its tests
    # Just look up all children at this stage
    for component_code in child_component_code_dict.values():
        extra_data = {
            "component" : component_code,
        }
        # Note that ITSDAQ electrical tests are assigend to the child hybrids for some reason

        logger.debug(f"module_summary_test (any): listTestRunsByComponent {extra_data['component']}")

        response = dbAccess.doSomething(
            "listTestRunsByComponent",
            method='GET',
            data=extra_data,
            token=token
        )
        # temp = response
        test_run_ids = test_run_ids + [test_run["id"] for test_run in response["pageItemList"]]


    # Get test runs by test run IDs
    extra_data = {
        "testRun" : test_run_ids
    }

    logger.debug(f"module_summary_test (remain): getTestRunBulk {extra_data['testRun']}")

    response = dbAccess.doSomething(
        "getTestRunBulk",
        method='GET',
        data=extra_data,
        token=token
    )
    # A list of all tests associated to a module
    module_tests = response["itemList"]
    temp = module_tests
    # Group tests by type
    tests_dict = {}
    for test in temp:
        test_type_code = test["testType"]["code"]
        if test_type_code not in tests_dict:
            tests_dict[test_type_code] = []
        tests_dict[test_type_code].append(test)
    # Now sort by date for each test
    for test_type_code, these_tests in tests_dict.items():
        # Sort each by test date, latest first
        tests_dict[test_type_code] = sorted(these_tests, key=lambda test: test["date"], reverse=True)
    # TODO: Better way to store required tests, but still
    temp = tests_dict

    # List of tests done on a module with SN 20USEM20000036
    # MODULE_IV_PS_V1 2
    # VISUAL_INSPECTION_RECEPTION 2
    # GLUE_WEIGHT 1
    # MODULE_METROLOGY 2
    # MODULE_BOW 1
    # VISUAL_INSPECTION 3
    # MODULE_WIRE_BONDING 1
    # MODULE_IV_PS_BONDED 1

    # Adding in children:
    # MANUFACTURING18 1
    # ASIC_GLUE_WEIGHT 1
    # ATLAS18_SHAPE_METROLOGY_V1 1
    # ATLAS18_MAIN_THICKNESS_V1 1
    # ATLAS18_VIS_INSPECTION_V2 1
    # ASIC_METROLOGY 1
    # ATLAS18_APPROVAL_V1 1
    # ATLAS18_IV_TEST_V1 1
    # MODULE_IV_PS_V1 2
    # VISUAL_INSPECTION_RECEPTION 3
    # GLUE_WEIGHT 1
    # MODULE_METROLOGY 2
    # MODULE_BOW 1
    # VISUAL_INSPECTION 3
    # MODULE_WIRE_BONDING 1
    # PEDESTAL_TRIM_PPA 2
    # STROBE_DELAY_PPA 2
    # RESPONSE_CURVE_PPA 4
    # NO_PPA 2 # Noise occupancy
    # MODULE_IV_PS_BONDED 1
    required_test_names = [
        "VISUAL_INSPECTION",
        "MODULE_METROLOGY",
        "MODULE_BOW",
        "GLUE_WEIGHT",
        "MODULE_WIRE_BONDING", # Construction tests complate
        "MODULE_IV_PS_V1",
        "MODULE_IV_PS_BONDED", # IV tests complete
        "PEDESTAL_TRIM_PPA",
        "STROBE_DELAY_PPA", 
        "NO_PPA", # Noise occupancy, electrical tests complete?
    ]

    
    # A total check of whether the children components have passed this test
    module_summary_test_result["results"]["all_required_subcomponents_present"] = False not in found_dict.values()
    # A dict to display whether all the needed components are attached
    module_summary_test_result["results"]["required_subcomponents_present"] = found_dict
    # A dict to display wherether all the required tests have been done
    module_summary_test_result["results"]["required_tests_done"] = dict([(required_test, required_test in tests_dict) for required_test in required_test_names])
    # A total check of whether all tests have been run
    module_summary_test_result["results"]["all_required_tests_done"] = False not in module_summary_test_result["results"]["required_tests_done"].values()
    # Check whether the last test of each required test type was done
    all_recent_required_tests_passed = None
    for test_name in required_test_names:
        if test_name in tests_dict:
            this_test = tests_dict[test_name][0]
            if this_test["passed"]:
                continue
            else:
                print(f"{test_name} failed")
                all_recent_required_tests_passed = False
                break
        else:
            print(f"{test_name} not present")
            all_recent_required_tests_passed = False
            break
    if all_recent_required_tests_passed is None:
        all_recent_required_tests_passed = True
    module_summary_test_result["results"]["all_recent_required_tests_passed"] = all_recent_required_tests_passed
    per_test_check = {}
    # Another idea, go through each required test, get most recent test, get whether it passed and if it failed report the commments/defects
    # per_test_check - tuple of whether test passed, comments, and test code
    for test_name in required_test_names:
        if test_name in tests_dict:
            this_test = tests_dict[test_name][0]
            if not this_test["passed"]:
                per_test_check[test_name] = (False, this_test["comments"], this_test["id"])
            else:
                per_test_check[test_name] = (True, None, this_test["id"])
    module_summary_test_result["results"]["per_test_check"] = per_test_check



    # Check for glue sample test, add glue sample name to summary
    
    
    # Now, for R0/R1/R2 we descend immediately, but for R3/R4/R5 we need to look at each submodule individually
    # TODO: split modules later, just look at the single sensor modules for now

    # TODO: Look up the stage of each component, check they're finished

    

    return module_summary_test_result, component_api_resp


def run_merge(base_result, top_module_api_response, top_module_tests_dict, left_module_summary_test_result, right_module_summary_test_result):
    """
    Shouldn't really need the API responses any more, hopefully

    A function that after submodule results are done merges the two together

    Mostly following the structure of the single module summary test
    
    Args:
        - base_result : The base module report result from previously in the report func, we use it here so we're not redoing some of the basic checks

    """

    module_summary_test_result = base_result

    # Add the code for the component
    module_summary_test_result["component"] = top_module_api_response["code"]
    # Assume we've had a good lookup
    module_summary_test_result["lookup_success"] = True

    # Check the top level module is in the finished state, and that the split modules are each in
    # the finished state
    if left_module_summary_test_result["results"]["module_finished_assembly"] and right_module_summary_test_result["results"]["module_finished_assembly"]:
        module_summary_test_result["results"]["module_finished_assembly"] = True
    else:
        module_summary_test_result["results"]["module_finished_assembly"] = False
    
    # Attach the comments of the children, and the top level
    if top_module_api_response["comments"] is not None:
        module_summary_test_result["comments"] = top_module_api_response["comments"] + left_module_summary_test_result["comments"] + right_module_summary_test_result["comments"]
    else:
        module_summary_test_result["comments"] = left_module_summary_test_result["comments"] + right_module_summary_test_result["comments"]

    # Child components logic test: this should come earlier I think
    module_summary_test_result["results"]["all_required_subcomponents_present"] = left_module_summary_test_result["results"]["all_required_subcomponents_present"] and right_module_summary_test_result["results"]["all_required_subcomponents_present"] 
    
    # Check all required tests done: 

    # module_summary_test_result["results"]["all_required_tests_done"] = left_module_summary_test_result["results"]["all_required_tests_done"] and right_module_summary_test_result["results"]["all_required_tests_done"]
    
    # Add required tests done for top level
    required_test_names = [
        "VISUAL_INSPECTION",
        "MODULE_WIRE_BONDING",
        "VISUAL_INSPECTION_RECEPTION", # Not sure if this should be neded
    ]
    # Forms a dict keyed by required test names that lists whether the test was done
    module_summary_test_result["results"]["required_tests_done"] = dict(
        [(required_test_name, required_test_name in top_module_tests_dict) for required_test_name in required_test_names]
    )

    # Add in the subtests, appending 'L_' for left and 'R_' for right
    for test_name, test_was_done in left_module_summary_test_result["results"]["required_tests_done"].items():
        module_summary_test_result["results"]["required_tests_done"]["L_" + test_name] = test_was_done
    for test_name, test_was_done in right_module_summary_test_result["results"]["required_tests_done"].items():
        module_summary_test_result["results"]["required_tests_done"]["L_" + test_name] = test_was_done



    # Finally, check all required tests run
    # That'll be the lower level, plus two top level (wire bonding, visual inspection?)
    # Could do via a bool and then checking the dict, but easiest just to check the dict
    # A total check of whether all tests have been run
    module_summary_test_result["results"]["all_required_tests_done"] = False not in module_summary_test_result["results"]["required_tests_done"].values()

    # Now, need to check that the most recent test run of each test type was successful
    all_recent_required_tests_passed = None
    for test_name in required_test_names:
        if test_name in top_module_tests_dict:
            this_test = top_module_tests_dict[test_name][0]
            if this_test["passed"]:
                continue
            else:
                print(f"{test_name} failed")
                all_recent_required_tests_passed = False
                break
        else:
            print(f"{test_name} not present")
            all_recent_required_tests_passed = False
            break
    if all_recent_required_tests_passed is None:
        all_recent_required_tests_passed = True
    module_summary_test_result["results"]["all_recent_required_tests_passed"] = all_recent_required_tests_passed

    # Create the merged per test dict, take the daughter module tests and then we'll do upper level as well
    per_test_check = {}
    for required_test_name, per_test_check_result in left_module_summary_test_result["results"]["per_test_check"].items():
        per_test_check["L_" + required_test_name] = per_test_check_result
    for required_test_name, per_test_check_result in right_module_summary_test_result["results"]["per_test_check"].items():
        per_test_check["R_" + required_test_name] = per_test_check_result
    # Now add the top level results
    for test_name in required_test_names:
        if test_name in top_module_tests_dict:
            this_test = top_module_tests_dict[test_name][0]
            if not this_test["passed"]:
                per_test_check[test_name] = (False, this_test["comments"], this_test["id"])
            else:
                per_test_check[test_name] = (True, None, this_test["id"])
        # TODO: Check whether I need to deal with missing tests here as well
    module_summary_test_result["results"]["per_test_check"] = per_test_check


    return module_summary_test_result
