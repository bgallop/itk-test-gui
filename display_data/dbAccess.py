#!/usr/bin/env python
import json
import sys

try:
    # Installed by default on lxplus
    import requests
except ModuleNotFoundError:
    print("Please install the requests module,")
    print("the equivalent of one of the following:")
    print("  pip install requests")
    print("  yum install python-requests")
    sys.exit(1)

try:
    from requests_toolbelt.multipart.encoder import MultipartEncoder
except ImportError:
    MultipartEncoder = None

from pprint import PrettyPrinter
pp = PrettyPrinter(indent = 1, width = 200)

# Fix the str (python3) versus basestring (Python2) issue
# See: https://stackoverflow.com/questions/11301138/how-to-check-if-variable-is-string-with-python-2-and-3-compatibility
try:
    basestring
except NameError:
    basestring = str

# Shouldn't be used outside this module
_AUTH_URL = 'https://uuidentity.plus4u.net/uu-oidc-maing02/bb977a99f4cc4c37a2afce3fd599d0a7/oidc/'
_SITE_URL = 'https://itkpd-test.unicorncollege.cz/'
_BIN_URL  = 'https://itkpd-test.unicorncollege.cz/uu-app-binarystore/'

# Define an exception specific to this file so that we may catch them (if we wish)
class dbAccessError(Exception):
    def __init__(self, message, *args, **kwargs):
        super(dbAccessError, self).__init__(*args, **kwargs)
        self.message = message
    def __str__(self):
        return self.message

# Recommended page sizes when fetching data (for pagination)
_UUCMD_LIST_PAGE_SIZE = {'listInstitutions': 3000, 'listComponents': 3000, 'listMyComponents': 3000, 'listTestRunsByTestType': 3000}
_UUCMD_BULK_PAGE_SIZE = {'getComponentBulk': {'page_size': 100, 'key': 'component'}, 'getTestRunBulk': {'page_size': 100, 'key': 'testRun'}}

verbose = False

token = None

def setupConnection(accessCode1 = None, accessCode2 = None):
    # global token

    print("Setup connection")

    token, failed = authenticate(accessCode1, accessCode2)
    return failed, token

def to_bytes(s):
    try:
        return bytes(s, 'utf-8')
    except TypeError:
        # Python 2, already OK
        return s

# DB has unicode, but console might be something else
# If eg ASCII, replace unicode chars
# If directed to file, force utf-8
def fix_encoding(s):
    enc = sys.stdout.encoding

    # Default to utf-8 if redirected
    if enc is None:
        enc = "utf-8"

    if sys.version_info[0] == 2:
        return s.encode(enc, "replace")
    else:
        # Encode string into bytes
        s = s.encode(enc, "replace")
        s = s.decode(enc, "replace")
    return s

def myprint(s, error = False):
    if not error:
        print(fix_encoding(s))
    else:
        sys.stderr.write(fix_encoding(s) + '\n')

def authenticate(accessCode1 = None, accessCode2 = None):
    sys.stderr.write("Getting token\n")
    # post
    # Everything is json header

    a = {"grant_type": "password"}

    a["accessCode1"] = accessCode1
    a["accessCode2"] = accessCode2
    a['scope'] = 'openid https://itkpd-test.unicorncollege.cz/'
    a = to_bytes(json.dumps(a))

    sys.stderr.write("Sending credentials to get a token\n")

    result = doSomething("grantToken", a, url = _AUTH_URL)
    # print("Authenticate result:", result)
    failed = False
    try:
        j = to_bytes(result)
        id_token = j["id_token"]
        sys.stderr.write("got token\n")
    except Exception:
        failed = True
        token = None
        sys.stderr.write("Problem with token\n")
        return token, failed
    return id_token, failed

def doMultiSomething(url, paramdata = None, method = None,
                     headers = None,
                     attachments = None):

    if verbose:
        print("Multi-part request to %s" % url)
        print("Send data: %s" % paramdata)
        print("Send headers: %s" % headers)
        print("method: POST")

    # print paramdata
    r = requests.post(url, data = paramdata, headers = headers,
                      files = attachments)

    if r.status_code in [500, 401]:
        print("Presumed auth failure")
        print(r.json())
        return None

    if r.status_code != 200:
        print(r)
        print(r.status_code)
        print(r.headers)
        print(r.text)
        r.raise_for_status()

    try:
        return r.json()
    except Exception as e:
        print("No json? ", e)
        return r.text

# Passed the uuAppErrorMap part of the message response
def decodeError(message, code):
    if "uu-app-server/internalServerError" in message:
        # Eg authentication problem "Signature verification raised"
        message = message["uu-app-server/internalServerError"]
        message = message["message"]
        print("Server responded with error message (code %d):" % code)
        myprint("\t%s" % message)
        return

    if message == "uu-appg01/authentication/invalidCredentials":
        sys.stderr.write("Server reported invalid credentials (code %d):\n" % code)
        return

    if "uu-oidc-main/notAuthenticated" in message:
        # Returned from the authentication attempt
        message = message["uu-oidc-main/notAuthenticated"]
        message = message["message"]
        sys.stderr.write("Server responded with error message (code %d):\n" % code)
        myprint("\t%s" % message, error=True)
        return

    found = False

    for k in message.keys():
        if "cern-itkpd-main" in k:
            if "componentTypeDaoGetByCodeFailed" in k:
                found = True
                print("Either component type is invalid, or nothing found")
                continue
            elif "invalidDtoIn" in k:
                print("Decoding error message in %s" % k)
                found = True
        else:
            continue

        info = message[k]

        if "paramMap" in info:
            paramInfo = info["paramMap"]
            if "missingKeyMap" in paramInfo:
                for (k, v) in paramInfo["missingKeyMap"].items():
                    if len(list(v.keys())) == 1:
                        reason = v[list(v.keys())[0]]
                    else:
                        myprint("%s" % v.keys())
                        reason = v

                    if "$" in k:
                        # Seem to have $. in front
                        param_name = k[2:]
                    else:
                        param_name = k
                    myprint("Key '%s' missing: %s" % (param_name, reason))
            # There's probably also a invalidValueKeyMap which might be useful
        else:
            myprint(str(info))

    if not found:
        myprint("Unknown error message: %s" % str(message))

def doRequest(url, data = None, headers = None, method = None):
    if method == "post" or method == "POST" or (method is None and data is not None):
        method = "POST"
    else:
        method = "GET"

    if verbose:
        print("Request to %s" % url)
        print("Send data %s" % data)
        print("Send headers %s" % headers)
        print("method %s" % method)

    if method == "POST":
        # print("Sending post")
        r = requests.post(url, data = data,
                          headers = headers)
    else:
        # print("Sending get")
        r = requests.get(url, data = data,
                         headers = headers)

    if r.status_code == 401:
        j = r.json()
        if "uuAppErrorMap" in j and len(j["uuAppErrorMap"]) > 0:
            if "uu-oidc/invalidToken" in j["uuAppErrorMap"]:
                global token
                print("Auth failure, need a new token!")
                token = None
                raise dbAccessError("Auth failure, token out of date")

    if "content-type" in r.headers:
        # Expect "application/json; charset=UTF-8"
        ct = r.headers["content-type"]
        if ct.split("; ")[0] != "application/json":
            myprint("Received unexpected content type: %s" % ct)
    else:
        print(r.headers)

    try:
        return r.json()
    except Exception as e:
        print("No json? ", e)
        return r.text

def doSomething(action, data = None, url = None, method = None,
                attachments = None, token = None):
    if url is None:
        baseName = _SITE_URL
    else:
        baseName = url

    baseName += action

    if attachments is not None:
        # No encoding of data, as this is passed as k,v pairs
        headers = {"Authorization": "Bearer %s" % token}
        return doMultiSomething(baseName, paramdata = data,
                                headers = headers,
                                method = method, attachments = attachments)

    if data is not None:
        if type(data) is bytes:
            reqData = data
        else:
            reqData = to_bytes(json.dumps(data))
        if url is None: # Default
            pass # print("data is: ", reqData)
    else:
        reqData = None

    headers = {'Content-Type' : 'application/json'}
    headers.update({"Accept-Encoding": "gzip, deflate"})
    # Header, token
    if token is not None:
        headers["Authorization"] = "Bearer %s" % token

    result = doRequest(baseName, data = reqData,
                       headers = headers, method = method)
    return result
