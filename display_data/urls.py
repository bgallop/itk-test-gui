from django.urls import path

from . import views

app_name = 'display_data'
urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.login_page, name='login'),
    path('login_token', views.login_token_page, name='login_token'),
    path('receiver', views.do_openid_receive, name='openid_receive'),
    path('CIDForm', views.get_component_ID, name='component ID form'),
    path('FetchInst', views.fetch_institutions, name='fetch insts'),
    path('listInst', views.list_institutions, name='list insts'),
    path('inst/<str:inst_code>', views.view_institution, name='view inst'),
    path('inst/<str:inst_code>/<str:ct_code>',
         views.view_institution_comp_type, name='view inst comp_type'),
    path('complist',
         views.view_comp_table, name='view comp list'),
    path('summary', views.cached_summary, name='summary of cached'),
    path('tests/<str:component_id_or_sn>', views.test_summary, name='test results'),
    path('test_detail/<str:component_id_or_sn>/<int:count>/', views.tests_details, name='detail'),
    path('upload', views.upload_test, name='upload form'),
    path('upload_successful', views.upload_successful, name='upload successful'),
    path('test_plotting', views.plotting_example, name='test plotting'),
    path('another_request/<str:serial>/', views.request_component_tests, name='another request'),
    path('component/<str:serial>', views.get_component_view, name='view component'),
    path('defect_tree_view/<str:component_id>', views.defect_tree_view, name='defect_tree_view'),
    path(
        'module_summary_test/<str:component_id>', views.module_summary_test_view,
        name="module_summary_test_view"
    ),
    path(
        'module_summary_test/<str:component_id>/download', views.module_summary_test_json_view,
        name="module_summary_test_json_view"
    ),
    path(
        'hybrid_summary_test/<str:component_id>', views.hybrid_summary_test_view,
        name="hybrid_summary_test_view"
    ),
]

# path function has two required arguments: route and view
# route: string that contains a URL pattern
# view: to call the specific view function
# kwargs: arbitrary keyword arguments can be passed in a directory to the target view
# name: name of the URL
