from display_data.models import TestSummary

import numpy as np

# {
#         "name": "SD_HI",
#         "description": "Strobe delay too high",
#         "properties": {"chip_bank": "away",
#                        "chip_index": 7,
#                        "channel": 123}},
#     {
#         "name": "HI_NOISE",
#         "description": "Noise measurement too high",
#         "properties": {"chip_bank": "away",
#                        "chip_index": 7,
#                        “channel": 123}},
#     {
#         "name": "LO_NOISE",
#         "description": "Noise measurement too low”,
#         "properties": {"chip_bank": "away",
#                        "chip_index": 7,
#                        "channel_from": 12,
#                        "channel_to": 21}}

def split_defect(d):
    # split defect list returned by Manufacturing Data ATLAS18 test
    # defects are returned like: '2-1208, 2-1209, 2-1210' segement_number-channel_number
    out = []
    string = ''
    for c in d:
        if c != ',':
            if c != ' ':
                string = string + c
        else:
            out.append(string)
            string = ''
    if string != '':
        out.append(string)
    return out

def get_segment_strip(defect):
    segment = int(defect[0])
    strip = int(defect[2:])
    return segment, strip

def mapping(strip, segment, component_ID):
    test_list = TestSummary.objects.filter(
        component_ID=component_ID,
        test_name='Manufacturing Data ATLAS18')
    channel = None
    readout_stream = None
    chip_index = None
    if test_list:
        test = test_list[0]
        type = test.type
        if type == 'ATLAS18LS Sensor':
            number_chips = 10
            if segment == 1:
                channel = 2560 - strip
                readout_stream = 1
                chip_index = int(np.floor(channel / 128)) - number_chips
            if segment == 2:
                channel = 1280 - strip
                readout_stream = 0
                chip_index = int(np.floor(channel / 128))

        if type == 'ATLAS18SS Sensor':
            if segment == 1:
                channel = 1280 - strip
                readout_stream = 1
                chip_index = int(np.floor(channel / 128))
            if segment == 2:
                channel = 1280 - strip
                readout_stream = 0
                chip_index = int(np.floor(channel / 128))
            if segment == 3:
                channel = strip - 1
                readout_stream = 0
                chip_index = int(np.floor(channel / 128))
            if segment == 4:
                channel = strip - 1
                readout_stream = 1
                chip_index = int(np.floor(channel / 128))

        if type == 'ATLAS18R0 Sensor':
            if segment == 1:
                channel = 1024 - strip
                readout_stream = 1
                chip_index = int(np.floor(channel / 128)) + 3
            if segment == 2:
                channel = 1024 - strip
                readout_stream = 0
                chip_index = int(np.floor(channel / 128)) + 3
            if segment == 3:
                channel = strip - 1
                readout_stream = 0
                chip_index = int(np.floor(channel / 128))
            if segment == 4:
                channel = strip - 1
                readout_stream = 1
                chip_index = int(np.floor(channel / 128))

        if type == 'ATLAS18R1 Sensor':
            if segment == 1:
                channel = 1280 - strip
                readout_stream = 1
                chip_index = int(np.floor(channel / 128)) + 1
            if segment == 2:
                channel = 1280 - strip
                readout_stream = 0
                chip_index = int(np.floor(channel / 128)) + 1
            if segment == 3:
                channel = strip - 1
                readout_stream = 0
                chip_index = int(np.floor(channel / 128))
            if segment == 4:
                channel = strip - 1
                readout_stream = 1
                chip_index = int(np.floor(channel / 128))

        if type == 'ATLAS18R2 Sensor':
            if segment == 1:
                if strip <= 1536/2:
                    channel = strip + int(1536/2) - 1
                    readout_stream = 1
                    chip_index = int(np.floor(channel / 128)) - 5
                else:
                    channel = strip - 1
                    readout_stream = 1
                    chip_index = int(np.floor(channel / 128))
            if segment == 2:
                if strip <= 1536/2:
                    channel = int(1536/2) - strip
                    readout_stream = 0
                    chip_index = int(np.floor(channel / 128)) - 5
                else:
                    channel = 1536 - strip
                    readout_stream = 0
                    chip_index = int(np.floor(channel / 128))

        if type == 'ATLAS18R3 Sensor':
            if segment == 1:
                channel = 895 - strip
                readout_stream = 1
                chip_index = int(np.floor(channel / 128)) + 4
            if segment == 2:
                channel = 895 - strip
                readout_stream = 0
                chip_index = int(np.floor(channel / 128)) + 4
            if segment == 3:
                channel = strip - 1
                readout_stream = 0
                chip_index = int(np.floor(channel / 128))
            if segment == 4:
                channel = strip - 1
                readout_stream = 1
                chip_index = int(np.floor(channel / 128))
        
        if type == 'ATLAS18R4 Sensor':
            if segment == 1:
                channel = 2048 - strip
                readout_stream = 1
                chip_index = int(np.floor(channel / 128))
            if segment == 2:
                channel = 1024 - strip
                readout_stream = 0
                chip_index = int(np.floor(channel / 128))

        if type == 'ATLAS18R5 Sensor':
            if segment == 1:
                channel = 2304 - strip
                readout_stream = 1
                chip_index = int(np.floor(channel / 128))
            if segment == 2:
                channel = 1152 - strip
                readout_stream = 0
                chip_index = int(np.floor(channel / 128))

    return channel, readout_stream, chip_index





class Defect_Mapping:
    def __init__(self):
        self.name = []
        self.description = []
        self.chip_bank = []
        self.chip_index = []
        self.channel = []
        self.strip = []
        self.segment = []
        self.test = []

    def get_defects_sensor(self, component_ID):
        test_list = TestSummary.objects.filter(
            component_ID=component_ID,
            test_name='Manufacturing Data ATLAS18')
        if test_list:
            test = test_list[0]
            defects = test.test_content['defects']
            if defects:
                no_defect_list = ['none', '-', '9-9999']
                no_defects_name = ['Percentage of NG Strips']
                for defect in defects:
                    if defect['description'] not in no_defect_list:
                        if defect['name'] not in no_defects_name:
                            defect_strip = defect['description']
                            defect_name = defect['name']
                            list = split_defect(defect_strip)
                            for defect in list:
                                segment, strip = get_segment_strip(defect)
                                self.name.append(defect_name)
                                self.segment.append(segment)
                                self.strip.append(strip)
                                self.description.append('-')
                                self.test.append('Manufacturing Data ATLAS18')

                                # do mapping
                                channel, readout_stream, chip_index = mapping(strip, segment, component_ID)
                                self.channel.append(channel)
                                self.chip_bank.append(readout_stream)
                                self.chip_index.append(chip_index)

    def get_defects_hybrid(self, component_ID):
        test_list = []
        # TODO: update list with real names
        hybrid_tests = ['Three Point Gain', 'Response Curve', 'Strobe Delay'] # list of tests to be considered for defects collection

        for test_to_be_considered in hybrid_tests:
            test_query = TestSummary.objects.filter(
                component_ID=component_ID,
                test_name=test_to_be_considered)
            if test_query:
                test_list.append(test_query[0])
        
        for test in test_list:
            defects = test.test_content['defects']
            for defect in defects:
                self.name.append(defect['name'])
                self.description.append(defect['description'])
                try:
                    self.strip.append(defect['properties']['strip'])
                except KeyError:
                    self.strip.append('-')
                self.chip_bank.append(defect['properties']['chip_bank'])
                self.chip_index.append(defect['properties']['chip_index'])
                self.segment.append('-')
                try:
                    self.channel.append(defect['properties']['channel'])
                except KeyError:
                    self.channel.append('-')
                self.test.append(test.test_name)











