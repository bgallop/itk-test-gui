"""
Result of qualification task by Sadia Marium

2023-08-09

"""

from display_data import dbAccess

from display_data.read_db import StandardCommand
import argparse

import json
import logging
from datetime import datetime, timezone
import numpy as np

logger = logging.getLogger(__name__)

def access(key1, key2):
    failed, token = dbAccess.setupConnection(accessCode1=key1, accessCode2=key2)
    return failed, token


def do_db_get(comm, data, token):
    out_data = dbAccess.doSomething(comm.action, method='get',
                                    data=data, token=token)
    return out_data

def get_test(t, componentID, child=False, child_layer=0, child_number=0, token=None, test_count=0):
    index = 0
    test_count=1
    testRunID=any
    short_summary_summary=[]
    extraData = {}
    defects=[]
    comments=[]
    
    for k in range(0,len(t['testRuns'])): # exclude deleted tests
      if t['testRuns'][k]['state'] == 'ready' :
        index = k 
        short_summary={}
        test_name = t['code']
        testRunID = t['testRuns'][index]['id']
        short_summary["testRunID"] = testRunID
        #short_summary["test_name"] = test_name
        componentID = componentID,
        child=child,
        child_layer=child_layer,
        child_number=child_number,
        SC = StandardCommand
        command = SC('getTestRun', ['testRun'])
        extraData['testRun'] = testRunID
        logger.debug(f"hybrid_summary_test (loop ready): getTestRun {testRunID}")
        test_output = do_db_get(command, extraData, token)

        passed=t['testRuns'][index]['passed']  
        stage = test_output['components'][0]['testedAtStage']['code']
        short_summary["passed"] = passed
        short_summary["testedAtStage"] = stage
        if test_name== 'ASIC_GLUE_WEIGHT' or test_name== 'ASIC_METROLOGY':
          if passed and test_output['problems']:
              short_summary["problems"]= test_output['problems'] 
        #   find the defects
        try:    
            dictdefects={}
            dictdefects['description'] = test_output['defects'][0]['description']
            dictdefects['name'] = test_output['defects'][0]['name']
            dictdefects['properties'] = test_output['defects'][0]['properties']
            defects.append(dictdefects)
        except KeyError:
            pass
        except IndexError:
            pass
        #   Find the comments
        try: 
          comments= test_output['comments']
          allcomments=[]
          if comments is None:
              comments = []
          for com in comments:
              allcomments.append(com['comment'])   
        except KeyError:
            pass
            
        if comments:
            short_summary["comments"] = allcomments
        #if defects:
        #    short_summary["defects"] = defects
        if defects:
            short_summary["numDefects"] = len(defects)
        test_count = test_count+1
        short_summary_summary.append(short_summary)

    return short_summary_summary

# get crucial parameters
def get_crucial_parameters(t, componentID, token, test_count=0):
    index = 0
    test_count=1
    for k in range(0, len(t['testRuns'])):
      if t['testRuns'][k]['state'] == 'ready':  # exclude deleted tests
        index = k
    test_count = test_count+1
    test_name = t['code']
    testRunID = t['testRuns'][index]['id']
    componentID = componentID,
    SC = StandardCommand
    command = SC('getTestRun', ['testRun'])
    extraData = {}
    extraData['testRun'] = testRunID
    logger.debug(f"hybrid_summary_test (cruicial): getTestRun {testRunID}")
    test_output = do_db_get(command, extraData, token)
    
    DEFECTS = []
    defect_ph=0 #package height
    defect_gh=0 #glue height
    defect_ASICmiss=0 #missing ASIC
    defect_err=0
    glue_weight = 0
    GH_HCC = []
    GH_ABC = []
    PH_HCC =[]
    PH_ABC = []
    AvgHeightValues=[]
    total_failed=0
    total_repaired=0
    e=''
    chips= {}
    chips['X']= ['ABC_X_0','ABC_X_1','ABC_X_2','ABC_X_3','ABC_X_4','ABC_X_5','ABC_X_6','ABC_X_7','ABC_X_8','ABC_X_9','HCC_X_0','Random Chip']
    chips['Y']= ['ABC_Y_0','ABC_Y_1','ABC_Y_2','ABC_Y_3','ABC_Y_4','ABC_Y_5','ABC_Y_6','ABC_Y_7','ABC_Y_8','ABC_Y_9','HCC_Y_0']
    chips['ROH0'] = ['ABC_R0H0_10','ABC_R0H0_9','ABC_R0H0_8','ABC_R0H0_7','ABC_R0H0_6','ABC_R0H0_5','ABC_R0H0_4','ABC_R0H0_3','HCC_R0H0_2']
    chips['R0H1'] = ['ABC_R0H1_0','ABC_R0H1_1','ABC_R0H1_2','ABC_R0H1_3','ABC_R0H1_4','ABC_R0H1_5','ABC_R0H1_6','ABC_R0H1_7','ABC_R0H1_8','HCC_R0H1_3']
    chips['R1H0'] = ['ABC_R1H0_1','ABC_R1H0_2','ABC_R1H0_3','ABC_R1H0_4','ABC_R1H0_5','ABC_R1H0_6','ABC_R1H0_7','ABC_R1H0_8','ABC_R1H0_9','ABC_R1H0_10','HCC_R1H0_4','HCC_Random']
    chips['R1H1'] = ['ABC_R1H1_0','ABC_R1H1_1','ABC_R1H1_2','ABC_R1H1_3','ABC_R1H1_4','ABC_R1H1_5','ABC_R1H1_6','ABC_R1H1_7','ABC_R1H1_8','ABC_R1H1_9','ABC_R1H1_10','HCC_R1H1_5']
    chips['R2H0'] = ['HCC_R2H0_3','HCC_R2H0_2','ABC_R2H0_11','ABC_R2H0_10','ABC_R2H0_9','ABC_R2H0_8','ABC_R2H0_7','ABC_R2H0_6','ABC_R2H0_5','ABC_R2H0_4','ABC_R2H0_3','ABC_R2H0_2','ABC_R2H0_1','ABC_R2H0_0']
    chips['R3H0'] = ['ABC_R3H0_10','ABC_R3H0_9','ABC_R3H0_8','ABC_R3H0_7','ABC_R3H0_6','ABC_R3H0_5','ABC_R3H0_4']
    chips['R3H1'] = ['HCC_R3H1_5','HCC_R3H1_4','ABC_R3H1_10','ABC_R3H1_9','ABC_R3H1_8','ABC_R3H1_7','ABC_R3H1_6','ABC_R3H1_5','ABC_R3H1_4']
    chips['R3H2'] = ['ABC_R3H2_0','ABC_R3H2_1','ABC_R3H2_2','ABC_R3H2_3','ABC_R3H2_4','ABC_R3H2_5','ABC_R3H2_6']
    chips['R3H3'] = ['HCC_R3H3_7','HCC_R3H3_6','ABC_R3H3_0','ABC_R3H3_1','ABC_R3H3_2','ABC_R3H3_3','ABC_R3H3_4','ABC_R3H3_5','ABC_R3H3_6']
    chips['R4H0'] = ['ABC_R4H0_10','ABC_R4H0_9','ABC_R4H0_8','ABC_R4H0_7','ABC_R4H0_6','ABC_R4H0_5','ABC_R4H0_4','ABC_R4H0_3']
    chips['R4H1'] = ['HCC_R4H1_3','HCC_R4H1_2','ABC_R4H1_10','ABC_R4H1_9','ABC_R4H1_8','ABC_R4H1_7','ABC_R4H1_6','ABC_R4H1_5','ABC_R4H1_4','ABC_R4H1_3']
    chips['R5H0'] = ['ABC_R5H0_10','ABC_R5H0_9','ABC_R5H0_8','ABC_R5H0_7','ABC_R5H0_6','ABC_R5H0_5','ABC_R5H0_4','ABC_R5H0_3','ABC_R5H0_2']
    chips['R5H1'] = ['ABC_R5H1_10','ABC_R5H1_9','ABC_R5H1_8','ABC_R5H1_7','ABC_R5H1_6','ABC_R5H1_5','ABC_R5H1_4','ABC_R5H1_3','ABC_R5H1_2','HCC_R5H1_5','HCC_R5H1_4']
    if test_name == "ASIC_GLUE_WEIGHT" :   
        glue_weight = test_output['results'][0]['value']

    if test_name == "ASIC_METROLOGY" :
        try:
            for m in test_output['results']:  
                if m['code'] == 'HEIGHT':
                    gheight = m['value']                
                if m['code'] == 'TOTAL_HEIGHT':
                    pheight = m['value']

            glue_height = gheight.values() 
            pakage_height = pheight.values() 
            
            for h, g, ph, p in zip(gheight, glue_height,pheight, pakage_height ): 
                 
                # list GH and PH values in results
                if "ABC" in h:
                    GH_ABC.append(round(g))
                    PH_ABC.append(round(p))
                    #round(PH_ABC)
                if "HCC" in h:
                    GH_HCC.append(round(g))
                    PH_HCC.append(round(p))

                # check package height
                pdef={}
                PHdefect={}
                if "ABC_X" in h:                # for barrel 
                    if (p>730 and p<800): 
                        defect_ph=str('package height below 800um')
                        pdef["ASIC"]= ph
                        pdef["value"]=p 
                        #PHdefect['name'] = 'PACKAGE_HEIGHT_OUT_OF_SPEC'
                        PHdefect['name'] = defect_ph
                        PHdefect['description']= "Package height must be between 800 and 880"
                        PHdefect['properties'] = pdef
                        DEFECTS.append(PHdefect)
                    elif p>880: 
                        defect_ph=str('package height over 880um')  
                        pdef["ASIC"]= ph
                        pdef["value"]=p 
                        PHdefect['name'] = defect_ph
                        PHdefect['description']= "Package height must be between 800 and 880"                      
                        PHdefect['properties'] = pdef
                        DEFECTS.append(PHdefect)
                    
                elif "ABC_R" in h:              # for endcap 
                    if p<650: 
                        defect_ph=str('package height below 650um')   
                        pdef["ASIC"]= ph
                        pdef["value"]=p
                        PHdefect['name'] = defect_ph
                        PHdefect['description']= "Package height must be between 650 and 730"
                        PHdefect['properties'] = pdef
                        DEFECTS.append(PHdefect)
                    elif (p>730 and p<800): 
                        defect_ph=str('package height over 730um')     
                        pdef["ASIC"]= ph
                        pdef["value"]=p 
                        PHdefect['name'] = defect_ph
                        PHdefect['description']= "Package height must be between 650 and 730"
                        PHdefect['properties'] = pdef
                        DEFECTS.append(PHdefect) 
              #check missing ASIC
                for dc in chips:                     
                    if dc in h: 
                        chip= chips[dc]   
                        for e in chip: 
                            if e not in gheight:  
                                defect_ASICmiss=str('missing an ASIC')   
            avgGH_ABC = np.mean(GH_ABC)
            avgGH_HCC = np.mean(GH_HCC)
            avgPH_ABC = np.mean(PH_ABC)
            avgPH_HCC = np.mean(PH_HCC)

            AvgHeightValues.append(avgGH_ABC)
            AvgHeightValues.append(avgGH_HCC)
            AvgHeightValues.append(avgPH_ABC)
            AvgHeightValues.append(avgPH_HCC)

              # check glue height 
            for g in (glue_height):
                if g<60: 
                    defectedASIC={}
                    GHdefect={}
                    defect_gh=str('glue height below 60 g')
                    defectedASIC['ASIC']=h
                    defectedASIC['height']=g
                    GHdefect['name'] = 'GH_OUT_OF_SPEC'
                    GHdefect['description'] = defect_gh
                    GHdefect['properties'] = defectedASIC
                    DEFECTS.append(GHdefect) 
        except Exception:
            defect_err=str('Some Error')
    if defect_err:
        PHdefect['description'] = defect_err
    if test_name == "WIRE_BONDING" :
        wire_bonds = test_output['results']
        for w in wire_bonds:
            if w['code'] == 'TOTAL_FAILED_ASIC_BACKEND':
                total_failed = w['value']
            if w['code'] == 'REPAIRED_ASIC_BACKEND':
                total_repaired = w['value']

    missingASIC={}
    if defect_ASICmiss: 
        missingASIC['name'] = 'MISSING_ASIC'
        missingASIC['description'] = defect_ASICmiss
        missingchip= {}
        missingchip['ASIC'] =e
        missingASIC['properties']= missingchip
        DEFECTS.append(missingASIC)

    return glue_weight, GH_HCC, GH_ABC, PH_HCC, PH_ABC, total_failed, total_repaired, DEFECTS, AvgHeightValues

def hybrid_summary_test(componentID, token):   
    test_count = 0
    SC = StandardCommand
    command = SC("getComponent", ["component"])
    extraData = {}
    extraData['component'] = componentID
    logger.debug(f"hybrid_summary_test: getComponent {componentID}")
    Out = do_db_get(command, extraData, token)
    
    componentID = Out['code']
    
    # what component do I have?


    current_stage = Out['currentStage'] ['name']

    print("component's current stage: ",current_stage)
    #   Get associated tests
    tests = Out ['tests']
    summary={}
    summary["component"] = componentID
    summary["testType"] =("HYBRID_SUMMARY_TEST")
    summary["institution"] = "HUBERLIN"
    summary["runNumber"] = ('1')
    summary["passed"] = True
    summary["date"] = datetime.now(timezone.utc).isoformat(timespec='milliseconds')
    summary["comments"]=[]

    performed_tests=[]
    comment =[]
    results={} 
    
    stages = Out['stages']
    been_through_stage=[]
    for st in stages:
      stage = st['code']
      been_through_stage.append(stage)
      #results['stages_the_hybrid_has_been_through']=been_through_stage
    stage1 = "ASIC_ATTACHMENT"
    test_list1 = ['ASIC_GLUE_WEIGHT', 'ASIC_METROLOGY'] 

    stage2 = 'WIRE_BONDING'
    test_list2 = ['NO_PPA','STROBE_DELAY_PPA','RESPONSE_CURVE_PPA','PEDESTAL_TRIM_PPA','WIRE_BONDING'] 
    
    stage3 = "BURN_IN"
    test_list3 = ['NO_PPA','STROBE_DELAY_PPA','RESPONSE_CURVE_PPA','PEDESTAL_TRIM_PPA','RESPONSE_CURVE_BURNIN'] 
    
    stage4 = 'FINISHED_HYBRID'
    test_list4 = ['NO_PPA','STROBE_DELAY_PPA','RESPONSE_CURVE_PPA','PEDESTAL_TRIM_PPA'] 
    
    stage5 = 'AT_MODULE_ASSEMBLY_SITE'
    test_list5 = ['NO_PPA','STROBE_DELAY_PPA','RESPONSE_CURVE_PPA','PEDESTAL_TRIM_PPA']

    stage6 = 'ON_MODULE'
    test_list6 =  ['NO_PPA','STROBE_DELAY_PPA','RESPONSE_CURVE_PPA','PEDESTAL_TRIM_PPA']

    stage_list = [stage1,stage2,stage3,stage4,stage5,stage6]
    test_list = [test_list1,test_list2,test_list3,test_list4,test_list5,test_list6]
    missing_stages =[]
    for stageitem in stage_list:
        if stageitem not in been_through_stage:
           missing_stages.append(stageitem)
           results["missing_stages"]=missing_stages

    results['testRunIDs']=[]
    resultList=[]
    various_defects=[]   

    for t in tests:          
        test_name = t['code']
        crucial_params= get_crucial_parameters(t, componentID, token, test_count)
        if test_name == "ASIC_GLUE_WEIGHT" :
            if crucial_params[0]:
                results["GlueWeight"]=round(crucial_params[0],4)  # glue_weight
        if test_name == "ASIC_METROLOGY" : 
            #results["ASIC_METROLOGY"] = hybrid_test_results
            results["GlueHeightsHCC"] = crucial_params[1] # GH_HCC
            results["GlueHeightsABC"] = crucial_params[2]  #GH_ABC
            results["PackageHeightHCC"] = crucial_params[3] # PH_HCC
            results["PackageHeightABC"] = crucial_params[4]  # PH_ABC
            results["Average_Glue_height_ABC"] = crucial_params[8][0]
            results["Average_Package_Height_ABC"] = crucial_params[8][2]
            if crucial_params[7]:         
                for c in crucial_params[7]:
                    various_defects.append(c)
                
        if test_name == "WIRE_BONDING" :
            if crucial_params[5]:
                results['Failed_Wire_bonds']=crucial_params[5]
            if crucial_params[6]:
                results['Repaired_Wire_bonds']=[crucial_params[6]]
        
        resultList=get_test(t, componentID, child=False, child_layer=0, child_number=0, token=token, test_count=test_count)        
        for newTest in resultList:
          results['testRunIDs'].append(newTest['testRunID'])
          results[test_name]=resultList

        #   CHECK IF CRUCIAL TESTS WERE PASSED  
        failed_tests={}
        for s in resultList:
          if not s['passed']:
            failed_tests["name"] = str(f"{test_name} failed at {s['testedAtStage']} stage")
            failed_tests["description"] = ''
            failed_tests["properties"] = {}
            various_defects.append(failed_tests)

          #   CHECK IF TESTS PASSED WITH DEFECTS
          if 'numDefects' in s:
              if s['passed']:
                  comment.append(str(f"{test_name} passed with defect at {(s['testedAtStage'])} stage"))

          performed_tests.append(test_name)

   #   CHECK IF ALL TESTS WERE PERFORMED AT VARIOUS STAGES
  
    for stage_item,test_item in zip(stage_list,test_list):
      if stage_item in been_through_stage:
        for tt in test_item:
          if tt not in performed_tests:

            missingtest={} 
            missingtest["name"]=str(f"{tt} was not performed at {stage_item} stage")
            missingtest["description"] = "Missing tests"
            missing_property={}
            missing_property["Missing test"] = tt
            missing_property["missing at stage"] = stage_item
            missingtest["properties"] = missing_property
            various_defects.append(missingtest)

    summary["defects"]=various_defects
    summary["comments"] = comment 
    summary["results"] =  results
    
    return summary

if __name__ == "__main__":
    """
    Main CLI entry point for Sadia's hybrid summary test.

    TODO: Fix this
    """
    parser = argparse.ArgumentParser(description="Extract test outputs")

    parser.add_argument("--component-id", help="Override component code")

    parser.add_argument("--verbose", action="store_true",
                        help="Print what's being sent and received")

    args = parser.parse_args()
    componentID = None
    if args.component_id:
        componentID = args.component_id
    #try:
    #
    token = None
    extract_tests = hybrid_summary_test(componentID, token)
    #filename = 'summary.json'
    filename = 'HybridSummaryTest.json'
    with open (filename, mode='w') as f:
        jsonString = json.dump(extract_tests, f, indent=2)
    #upload_script = 'upload_test_results.py'
    #generated_file = 'summary.json'
    #subprocess.run(['python3', upload_script, '--test-file', generated_file])
        
    #except: 
    #    print("Error")
