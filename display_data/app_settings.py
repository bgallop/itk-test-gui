"""
Application settings, such as whether to show debug info or not etc
"""

import os

SHOW_MODULE_SUMMARY_DEBUG = False

OPENID_CLIENT_ID = None
OPENID_CLIENT_SECRET = None

if os.getenv("MY_OPENID_SECRET"):
    OPENID_CLIENT_SECRET = os.getenv("MY_OPENID_SECRET")
if os.getenv("MY_OPENID_ID"):
    OPENID_CLIENT_ID = os.getenv("MY_OPENID_ID")
