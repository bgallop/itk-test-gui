from django import forms
from django.forms.widgets import CheckboxInput

class ComponentIDForm(forms.Form):
    without_request = forms.BooleanField(label='check local database first', widget=CheckboxInput, required=False)
    Component_ID = forms.CharField(label='ATLAS serial number or component ID', max_length=100, required=True)

class AccessForm(forms.Form):
    key1 = forms.CharField(label='access key 1', widget=forms.PasswordInput)
    key2 = forms.CharField(label='access key 2', widget=forms.PasswordInput)

    # Optionally allow redirect to previous page
    redirect = forms.CharField(max_length = 100, required = False,
                               widget=forms.HiddenInput)

class TokenForm(forms.Form):
    db_token = forms.CharField(max_length = 2000)

class Upload_Test(forms.Form):
    Component_ID = forms.CharField(label='component ID', max_length=100)
    comments = forms.CharField(label='comments', max_length=300)
    institution = forms.CharField(label='institution', max_length=100)
    file = forms.FileField(label='.JSON file')

class ListFilterForm(forms.Form):
    """
    Form to store/update data on filter for listComponents.

    The current filter information is hidden, and new items can be added.
    """

    def __init__(self, *args, choices=None, **kwargs):
        super(ListFilterForm, self).__init__(*args, **kwargs)

        if choices:
            if "comp_pick" in choices:
                self.fields["comp_pick"].choices = choices["comp_pick"]

    comp_pick = forms.ChoiceField(label="Select component type", widget=forms.Select, required = False)

class OpenidForm(forms.Form):
    access_token = forms.CharField(max_length = 1000)
    session_expires_in = forms.DecimalField(max_digits = 6)
    id_token = forms.CharField(max_length = 2000)
    session_state = forms.CharField(max_length = 128)
    token_type = forms.CharField(max_length = 10)
    expires_in = forms.DecimalField(max_digits = 6)
