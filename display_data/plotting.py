from display_data.do_plotting_stuff import (
    get_plot, get_plot_2, get_plot_errorbar, get_plot_scatter
)

def three_point_gain(results):
    r = results
    p = []

    #plotting
    p.append(get_plot_errorbar(
        y0=r["STREAM0_VT50"], y1=r["STREAM1_VT50"],
        y0_err=r["STREAM0_VT50_RMS"], y1_err=r["STREAM1_VT50_RMS"],
        ylabel='VT50'))
    p.append(get_plot_errorbar(
        y0=r["STREAM0_GAIN"], y1=r["STREAM1_GAIN"],
        y0_err=r["STREAM0_GAIN_RMS"], y1_err=r["STREAM1_GAIN_RMS"],
        ylabel='GAIN'))
    p.append(get_plot_errorbar(
        y0=r["STREAM0_OFFSET"], y1=r["STREAM1_OFFSET"],
        y0_err=r["STREAM0_OFFSET_RMS"], y1_err=r["STREAM1_OFFSET_RMS"],
        ylabel='OFFSET'))
    p.append(get_plot_errorbar(
        y0=r["STREAM0_INNSE"], y1=r["STREAM1_INNSE"],
        y0_err=r["STREAM0_INNSE_RMS"], y1_err=r["STREAM1_INNSE_RMS"],
        ylabel='INNSE'))
    p.append(get_plot_2(y0=r["STREAM0_OUTNSE"], y1=r["STREAM1_OUTNSE"], ylabel='OUTNSE'))
    p.append(get_plot_2(y0=r["STREAM0_P0"], y1=r["STREAM1_P0"], ylabel='P0'))
    p.append(get_plot_2(y0=r["STREAM0_P1"], y1=r["STREAM1_P1"], ylabel='P1'))
    return p

def response_curve(results):
    r = results
    p = []

    #plotting
    p.append(get_plot_errorbar(
        y0=r["STREAM0_VT50"], y1=r["STREAM1_VT50"],
        y0_err=r["STREAM0_VT50_RMS"], y1_err=r["STREAM1_VT50_RMS"],
        ylabel='VT50'))
    p.append(get_plot_errorbar(
        y0=r["STREAM0_GAIN"], y1=r["STREAM1_GAIN"],
        y0_err=r["STREAM0_GAIN_RMS"], y1_err=r["STREAM1_GAIN_RMS"],
        ylabel='GAIN'))
    p.append(get_plot_errorbar(
        y0=r["STREAM0_OFFSET"], y1=r["STREAM1_OFFSET"],
        y0_err=r["STREAM0_OFFSET_RMS"], y1_err=r["STREAM1_OFFSET_RMS"],
        ylabel='OFFSET'))
    p.append(get_plot_errorbar(
        y0=r["STREAM0_INNSE"], y1=r["STREAM1_INNSE"],
        y0_err=r["STREAM0_INNSE_RMS"], y1_err=r["STREAM1_INNSE_RMS"],
        ylabel='INNSE'))
    p.append(get_plot_2(y0=r["STREAM0_OUTNSE"], y1=r["STREAM1_OUTNSE"], ylabel='OUTNSE'))
    p.append(get_plot_2(y0=r["STREAM0_P0"], y1=r["STREAM1_P0"], ylabel='P0'))
    p.append(get_plot_2(y0=r["STREAM0_P1"], y1=r["STREAM1_P1"], ylabel='P1'))
    p.append(get_plot_2(y0=r["STREAM0_P2"], y1=r["STREAM1_P2"], ylabel='P1'))
    return p

def strobe_delay(results):
    r = results
    p = []

    #plotting
    p.append(get_plot_2(y0=r["STREAM0_DELAYS"], y1=r["STREAM1_DELAYS"], ylabel='DELAYS'))
    return p

def full_strip_test(results):
    r = results

    x = r['PROBEINDEX']
    R = r['RESISTANCE']
    I = r['CURRENT']
    C = r['CAPACITANCE']

    p = []
    p.append(get_plot(x=x, y=R, xlabel='strip number', ylabel='resistance [M$\Omega$]', figsize=(9, 3), markersize=1))
    p.append(get_plot(x=x, y=I, xlabel='strip number', ylabel='current [nA]', figsize=(9, 3), markersize=1))
    p.append(get_plot(x=x, y=C, xlabel='strip number', ylabel='capacitance [pF]', figsize=(9, 3), markersize=1))

    return p

def atlas18_cv_test(results):
    r = results

    R = r['RESISTANCE']
    V = r['VOLTAGE']
    C = r['CAPACITANCE']

    p = []
    p.append(get_plot(x=V, y=C, xlabel='voltage [V]', ylabel='capacitance [pF]'))
    p.append(get_plot(x=V, y=R, xlabel='voltage [V]', ylabel='resistance [k$\Omega$]'))

    return p

def manufactering_data_atlas18(results):
    r = results

    U = r['IV_VOLTAGE']
    I = r['IV_CURRENT']

    p = []
    p.append(get_plot(x=U, y=I, xlabel='voltage [V]', ylabel='current [A]', figsize=(5, 3)))

    return p

def atlas18_shape_metrology(results):
    r = results

    x = r['X']
    y = r['Y']
    z = r['Z']
    z_bow = r['Z_BOW']

    p = []
    p.append(get_plot_scatter(x=x, y=y, z=z, clabel='z [mm]'))
    p.append(get_plot_scatter(x=x, y=y, z=z_bow, clabel='z_bow'))

    return p
