# Generated by Django 4.2.14 on 2025-01-13 17:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('display_data', '0024_statscounter'),
    ]

    operations = [
        migrations.AddField(
            model_name='componentinfo',
            name='alternative_ID',
            field=models.CharField(max_length=40, null=True),
        ),
        migrations.AddField(
            model_name='componentinfo',
            name='batch_name',
            field=models.CharField(max_length=40, null=True),
        ),
        migrations.AddField(
            model_name='componentinfo',
            name='current_stage',
            field=models.CharField(max_length=40, null=True),
        ),
    ]
