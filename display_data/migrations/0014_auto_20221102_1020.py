# Generated by Django 3.2.1 on 2022-11-02 09:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('display_data', '0013_auto_20221101_0830'),
    ]

    operations = [
        migrations.AlterField(
            model_name='componentinfo',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
        migrations.AlterField(
            model_name='institute',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
        migrations.AlterField(
            model_name='testsummary',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
