from django.contrib import admin

from .models import ComponentInfo, Institute, TestSummary
from . import models    

class ModuleSummaryTestAdmin(admin.ModelAdmin):
    """
    To be able to see the datetime added field
    """
    readonly_fields = ('datetime_created',)

admin.site.register(ComponentInfo)
admin.site.register(Institute)
admin.site.register(TestSummary)
admin.site.register(models.ModuleSummaryTest, ModuleSummaryTestAdmin)

admin.site.register(models.ComponentData)
admin.site.register(models.StatsCounter)
