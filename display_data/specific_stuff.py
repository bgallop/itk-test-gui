
# Codes for all component types (with serial numbers) in a module
interesting_ct_codes = [
    "ABC", "HCC", "AMAC",

    "MODULE",
    "HYBRID_FLEX",
    "HYBRID_ASSEMBLY",
    "HVMUX", "BPOL12V",
    "PB_FLEX", "PWB", "PWB_COIL", "SENSOR",
]

# Only show institutes that interact with these components
interesting_top_level_codes = [
    "MODULE",
    "HYBRID_ASSEMBLY",
]
