from django.db import models
import datetime
from django.utils import timezone
from django.core.validators import MinLengthValidator

def dbIDField(unique=False, null=False):
    return models.CharField(validators=[MinLengthValidator(32)], max_length=32, unique=unique, null=null)

# An institute (first so it can be referred to later)
class Institute(models.Model):
    inst_id = dbIDField(unique=True)
    inst_code = models.CharField(max_length=40, unique=True)
    inst_name = models.CharField(max_length=40)

    def __str__(self):
        return self.inst_name
    pub_date = models.DateTimeField('date published')

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently'

class TestSummary(models.Model):
    test_name = models.CharField(max_length=40)
    passed = models.BooleanField(default=False)
    test_id = dbIDField()
    test_content = models.JSONField()
    component_type = dbIDField()
    type = models.CharField(max_length=40, default='')
    serialNumber = models.CharField(max_length=40, default='')
    component_ID = dbIDField()
    child_number = models.IntegerField(default=0)
    child = models.BooleanField(default=False)
    child_layer = models.IntegerField(default=0)
    testedAtStage = models.CharField(max_length=40, default='')
    count = models.IntegerField(default=0)

    def __str__(self):
        return "%s for %s" % (self.test_name, self.serialNumber)

    pub_date = models.DateTimeField('date published')

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently'

    associated_component = models.ForeignKey(
        "ComponentInfo",
        on_delete=models.CASCADE,
        null=False,
        blank=False
    )

# This is information on an instance of a particular component
class ComponentInfo(models.Model):
    component_ID = dbIDField(unique=True)
    # Allow serials to be null, rare but happens. See STAR hybrid flex
    serialNumber = models.CharField(max_length=40, default='', null=True)
    component_type = models.CharField(max_length=40, default='')
    type = models.CharField(max_length=40, default='')

    alternative_ID = models.CharField(max_length=40, null=True)
    current_stage = models.CharField(max_length=40, null=True)
    batch_name = models.CharField(max_length=40, null=True)

    # Allow parent ID to be null if parents don't exist, not just ''
    parents_ID = dbIDField(null=True)
    parents_type = models.CharField(max_length=40, default='', null=True)
    parents_serial = models.CharField(max_length=40, default='', null=True)
    nChildren = models.IntegerField(default=0)

    # Create parenthood relationship, which will also be used in reverse to get children
    # Allowed to be null in database, and allow to be blank
    # on_delete: Assume at this stage we won't be hotswapping components, thus cascading deletes are appropriate. Ie. on delete cascade, if the parent is deleted this child is created
    parent_component = models.ForeignKey(
        "self", 
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    def __str__(self):
        return "%s of type %s" % (self.serialNumber, self.component_type)

    pub_date = models.DateTimeField('date published')

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently'

class ModuleSummaryTest(models.Model):
    """
    Basic model for storing the module summary test.

    For simplicity's sake, this 
    """
    parent_component = models.OneToOneField(
        "ComponentInfo", # Associated to a component
        on_delete=models.CASCADE, # If parent is deleted, delete this
        null=False, # Must be associated to a component
        blank=False, # Must be associated to a component
    )

    summary_test_content = models.JSONField() # Field for summary test
    datetime_created = models.DateTimeField(auto_now=True) # Automatically set date
    # Consider adding a hash or something else


# This is information on an instance of a particular component
class ComponentData(models.Model):
    component_ID = dbIDField(unique=True)

    list_component_object = models.JSONField(null=True)
    get_component_full = models.JSONField(null=True)

    pub_date = models.DateTimeField('date published')

    # Mainly for debugging don't keep around very long
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(minutes=20) <= self.pub_date <= now

    def __str__(self):
        return f"{self.component_ID[:4]}..{self.component_ID[-4:]} {self.list_component_object is not None} {self.get_component_full is not None}"

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently'

class StatsCounter(models.Model):
    stat_name = models.CharField(max_length=32, unique=True)
    stat_counter = models.IntegerField(default = 0)

    def __str__(self):
        return f"{self.stat_name} ({self.stat_counter})"

TestSummary.objects.get_queryset()
