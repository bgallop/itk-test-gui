# Quick Dockerfile for building the current app

FROM ubuntu:latest

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y python3 python3-pip

COPY . /itk-test-gui
WORKDIR /itk-test-gui

RUN pip3 install -r requirements.txt
EXPOSE 8000
ENTRYPOINT ["python3", "manage.py", "runserver", "0.0.0.0:8000"]