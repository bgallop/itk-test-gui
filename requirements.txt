asgiref==3.5.2
certifi==2020.12.5
chardet==4.0.0
cycler==0.10.0
Django==3.2.1
idna==2.10
kiwisolver==1.3.1
matplotlib==3.3.4
numpy>=1.0, <2.0
Pillow==10.2.0
pyparsing==2.4.7
python-dateutil==2.8.1
pytz==2020.4
requests==2.25.1
six==1.15.0
sqlparse==0.4.1
urllib3==1.26.2
gunicorn